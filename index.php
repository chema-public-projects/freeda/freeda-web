<?php
error_reporting(E_ERROR);
include_once "libs/analyticstracking.php";
session_start();
//session_destroy();
    if(!$_SESSION['TIPO_USUARIO']){
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SGAC | Sistema de Gestión de Actividades Complementarias</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="views/main/css/bootstrap.css">
    <!--Custom Styles-->
    <link rel="stylesheet" type="text/css" href="views/main/css/global.css">
    <link rel="stylesheet" type="text/css" href="views/main/css/loader.css">

    <!-- Custom Fonts -->


    <!-- Theme CSS -->
    <link rel="stylesheet"  href="views/main/css/theme.css" >

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you v1 the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->
    <link rel="stylesheet"  href="views/main/css/index.css">
</head>

<body id="page-top" class="index">
    <div id="loading">
      <div id="pantallaCompletaTransparente">
        <div class="loader" style="width:90px; margin: 300px auto;"></div>
      </div>
    </div>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">SGAC</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li><a class="page-scroll" href="#login">Ingresar</a></li>
                    <!--li><a class="page-scroll" href="#infografias">Ayuda</a></li>
                    <li><a class="page-scroll" href="#about">Acerca de</a></li-->
                    <li><a class="page-scroll" href="#services">Información Institucional</a></li>
                    <!--li><a class="page-scroll" href="#team">Desarrolladores</a></li-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
  <header>
    <div class="black-opacity">
      <div class="container">
        <div class="intro-text">
        <div class="intro-heading">Bienvenido</div>
        <div class="intro-lead-in">Sistema de Gestión de Actividades Complementarias</div>
        <!--a href="#services" class="page-scroll btn btn-xl">Ingresar</a-->
        <a href="#login" class="page-scroll btn btn-xl">Ingresar</a>
        </div>
      </div>
    </div>
  </header>

        <!-- INGRESAR -->
    <section id="login" class="bg-light-gray">
        <div class="container"  style=" padding: 20px; border-radius:10px; border: solid 3px #999; border-style: dotted;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Iniciar sesión</h2>
                    <h3 class="section-subheading text-muted">Puedes ingresar el número de control con mayúsculas o minúsculas.</h3>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="login" id="login-form" onsubmit="iniciarSesion()" return="false">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div id="mensaje" style="color: #f00; text-align: center;"></div><br>
                                <div class="form-group">
                                    <input id="usuario" type="text" class="form-control" placeholder="NÚMERO DE CONTROL" required data-validation-required-message="Ingresa tu número de control" style="text-align: center;">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input id="clave" type="password" class="form-control" placeholder="CONTRASEÑA" required data-validation-required-message="Ingresa tu contraseña" style="text-align: center;">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl" id="btn_login">ACEPTAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section><!--END LOGIN-->

    <!-- AYUDA -->
    <!--section id="infografias">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Ayuda</h2>
                    <h3 class="section-subheading text-muted">A continuación se muestran algunos puntos que pueden serte útiles. Selecciona la opción que mejor te parezca.<s/h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal1" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/roundicons.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>Primer ingreso</h4>
                        <p class="text-muted">Para los nuevos usuarios</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal2" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/startup-framework.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>Seguridad</h4>
                        <p class="text-muted">Cuida tu información</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal3" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/treehouse.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>Panel pricipal</h4>
                        <p class="text-muted">Conoce tu espacio</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal4" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/golden.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>FUncionalidades especcíficas</h4>
                        <p class="text-muted">Puede serte útil</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal5" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/escape.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>Mapa del sitio</h4>
                        <p class="text-muted"></p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 infografias-item">
                    <a href="#infografiasModal6" class="infografias-link" data-toggle="modal">
                        <div class="infografias-hover">
                            <div class="infografias-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="views/main/img/infografias/dreams.png" class="img-responsive" alt="">
                    </a>
                    <div class="infografias-caption">
                        <h4>FAQs Frecuent Asked Questios</h4>
                        <p class="text-muted">Preguntas Frecuentemente Preguntadas</p>
                    </div>
                </div>
            </div>
        </div>
    </section--><!--END AYUDA-->

    <!-- ACERCA DE  -->
    <!--section id="about" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Acerca de</h2>
                    <h3 class="section-subheading text-muted">Conoce qué es esto y cómo surge</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="views/main/img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2009-2011</h4>
                                    <h4 class="subheading">Our Humble Beginnings</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="views/main/img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>March 2011</h4>
                                    <h4 class="subheading">An Agency is Born</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="views/main/img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>December 2012</h4>
                                    <h4 class="subheading">Transition to Full Service</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="views/main/img/about/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>July 2014</h4>
                                    <h4 class="subheading">Phase Two Expansion</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Be Part
                                    <br>Of Our
                                    <br>Story!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section--><!--END ACERCA DE -->

    <!-- INFO INSTITUCIONAL -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Instituto Tecnológico Superior de Zongolica</h2>
                    <h3 class="section-heading">Nos transformamos con identidad</h3>
                    <img src="views/main/img/itsz.jpg">
                    <h3 class="section-subheading text-muted">Esta plataforma ha sido creada por alumnos del ITSZ. </h3>
                </div>
            </div>
            <!--div class="row text-center">
                <div class="col-md-12"><h3>Agradecimientos a...</h3></div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">El Profe Lorenzo</h4>
                    <p class="text-muted">Experto en PHP y servidores</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">El Checko Ixma</h4>
                    <p class="text-muted">Soporte de jQuery, WebServices, Diseño web y modelado de bases de datos</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">El Raygoza</h4>
                    <p class="text-muted">Por la resolución de situaciones imposibles con problemas técnicos.</p>
                </div>
            </div-->
        </div>
    </section><!--END INSTITUCIONAL-->

    <!-- DESARROLLADORES-->
    <!--section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Equipo de Desarrollo</h2>
                    <h3 class="section-subheading text-muted">El trabajo se hace bien o no se hace. Por fortuna esto no es un trabajo.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="views/main/img/team/1.jpg" class="img-responsive img-circle" alt="">
                        <h4>Kay Garland</h4>
                        <p class="text-muted">Lead Designer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="views/main/img/team/2.jpg" class="img-responsive img-circle" alt="">
                        <h4>Larry Parker</h4>
                        <p class="text-muted">Lead Marketer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="views/main/img/team/3.jpg" class="img-responsive img-circle" alt="">
                        <h4>Diana Pertersen</h4>
                        <p class="text-muted">Lead Developer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p class="large text-muted">
                        Hicimos nuestro mejor esfuerzo. Ojalá les sirva de algo.
                    </p>
                </div>
            </div>
        </div>
    </section--><!--END DESARROLLADORES-->





    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Todos los derechos reservados &copy; complementarias.itszongolica.edu.mx 2017</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="http://www.itszongolica.edu.mx">Página oficial del ITSZ</a>
                        </li>
                        <!--li><a href="#">Terms of Use</a-->
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
















<!-- ---------------------------------------INFOGRAFÍAS----------------------------------------------------------------------------------------------- -->

    <!-- infografias Modals -->
    <!-- Use the modals below to showcase details about your infografias projects! -->

    <!-- infografias Modal 1 -->
    <div class="infografias-modal modal fade" id="infografiasModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/roundicons-free.png" alt="">
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <p>
                                    <strong>Want these icons in this infografias item sample?</strong>You can download 60 of them for free, courtesy of <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">RoundIcons.com</a>, or you can purchase the 1500 icon set <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">here</a>.</p>
                                <ul class="list-inline">
                                    <li>Date: July 2014</li>
                                    <li>Client: Round Icons</li>
                                    <li>Category: Graphic Design</li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- infografias Modal 2 -->
    <div class="infografias-modal modal fade" id="infografiasModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>Project Heading</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/startup-framework-prev1.png" alt="">
                                <p><a href="http://designmodo.com/startup/?u=787">Startup Framework</a> is a website builder for professionals. Startup Framework contains components and complex blocks (PSD+HTML Bootstrap themes and templates) which can easily be integrated into almost any design. All of these components are made in the same style, and can easily be integrated into projects, allowing you to create hundreds of solutions for your future projects.</p>
                                <p>You can prev1 Startup Framework <a href="http://designmodo.com/startup/?u=787">here</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- infografias Modal 3 -->
    <div class="infografias-modal modal fade" id="infografiasModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/treehouse-prev1.png" alt="">
                                <p>Treehouse is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. This is bright and spacious design perfect for people or startup companies looking to showcase their apps or other projects.</p>
                                <p>You can download the PSD template in this infografias sample item at <a href="http://freebiesxpress.com/gallery/treehouse-free-psd-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- infografias Modal 4 -->
    <div class="infografias-modal modal fade" id="infografiasModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/golden-prev1.png" alt="">
                                <p>Start Bootstrap's Agency theme is based on Golden, a free PSD website template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Golden is a modern and clean one page web template that was made exclusively for Best PSD Freebies. This template has a great infografias, timeline, and meet your team sections that can be easily modified to fit your needs.</p>
                                <p>You can download the PSD template in this infografias sample item at <a href="http://freebiesxpress.com/gallery/golden-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- infografias Modal 5 -->
    <div class="infografias-modal modal fade" id="infografiasModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/escape-prev1.png" alt="">
                                <p>Escape is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Escape is a one page web template that was designed with agencies in mind. This template is ideal for those looking for a simple one page solution to describe your business and offer your services.</p>
                                <p>You can download the PSD template in this infografias sample item at <a href="http://freebiesxpress.com/gallery/escape-one-page-psd-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- infografias Modal 6 -->
    <div class="infografias-modal modal fade" id="infografiasModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-responsive img-centered" src="views/main/img/infografias/dreams-prev1.png" alt="">
                                <p>Dreams is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Dreams is a modern one page web template designed for almost any purpose. It’s a beautiful template that’s designed with the Bootstrap framework in mind.</p>
                                <p>You can download the PSD template in this infografias sample item at <a href="http://freebiesxpress.com/gallery/dreams-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="views/main/js/jquery/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="views/main/js/bootstrap.js"></script>
    <!-- Plugin JavaScript -->
    <script src="views/main/js/js-pluggin.js"></script>
    <!-- Contact Form JavaScript -->
    <script src="views/main/js/jqBootstrapValidation.js"></script>
    <script src="views/main/js/contact_me.js"></script>
    <!-- Theme JavaScript -->
    <script src="views/main/js/theme.js"></script>
    <!--CONTROLLERS-->
    <script src="views/main/js/enumeraciones.js"></script>
    <script src="views/main/js/validator.js"></script>
    <script src="views/main/js/utils.js"></script>
    <script src="views/alumnos/js/index.js"></script
</body>

</html>
<?php
    }
    else{
        switch($_SESSION['TIPO_USUARIO']){
            case "ALUMNO":
                echo '<script>window.location="views/alumnos/."</script>';
            break;//END CASE
            case "ADMINISTRADOR":
                echo '<script>window.location="views/administradores/."</script>';
            break;//END CASE
            case "INSTRUCTOR":
                echo '<script>window.location="views/instructores/."</script>';
            break;//END CASE
        }//END SWITCH
    }
?>
