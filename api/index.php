<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require 'modules/routes.php';
require "modules/ac/middleware.php";

$app = new \Slim\App;
$ActividadesComplementarias = new ActividadesComplementarias();

$middleware = function ($request, $response, $next){
        $response->getBody()->write(json_encode(["some" => "value 2"]));
        /*
            Este middleware se ejecuta primero
            Esta linea devuelve el control al invocador por un momento y después continúa ejecutandose el código de esta función
            $response = $next($request, $response);
        */
        return $response;
    };
/*
$app->add(function($req, $res, $next) {
    $res->getBody()->write('before');
    $res = $next($req, $res);
    $res->getBody()->write('after');

    return $res;
});*/

$app->get('/', function ($request, $response, $args) { 
    //$response->getBody()->write(json_encode(["some" => "value"]));
 })->add($middleware);

$app->get('/ac', $ActividadesComplementarias->getActividades());

$app->run();

?>