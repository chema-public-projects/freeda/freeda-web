<?php

/**
 * 
 */
class Http {
   
    /**
     * 
     */
    public function __construct(){
    }

    /**
     * 
     */
    public function getHttpCodes(){
        $codes = [
            "OK" => 200,
            "CREATED" => 204,
            "ACCEPTED" => 202,
            "BAD_REQUEST" => 400,
            "UNAUTHORIZED" => 401,
            "FORBIDEN" => 403,
            "NOT_FOUND" => 404,
            "INTERNAL_SERVER_ERROR" => 500,
            "SERVICE_UNAVAILABLE" => 503
        ];
        return $codes;
    }
}

/**
 * 
 */
class Messages{
    /**
     * 
     */
    public function __construct(){
        /**
         * NOT IMPLEMENTED YET
         */
    }

    /**
     * 
     */
    public function getResponseStructure(
            $strStatus,
            $intCode,
            $strMsg,
            $strDetails,
            $intAffectedRows,
            $arrData
        ) {
        $responseStructure = [
            "status" => $strStatus,
            "code" => $intCode,
            "msg" => $strMsg,
            "details" => $strDetails,
            "affected_rows" => $intAffectedRows,
            "data" => $arrData
        ];
        return $responseStructure;
    }
}

/**
 * 
 */
class Utils{
    public $Http;
    public $Messages;

    public function __construct(){
        $this->Http = new Http();
        $this->Messages = new Messages();
    }
}

?>