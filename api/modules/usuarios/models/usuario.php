<?php
$modules_path = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
require $modules_path.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'utils.php';
require $modules_path.DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'mysql.php';

class Usuario{
    private $Utils;

    public $intIdUsuario = 0;
    public $strPass = "";
    public $strNombreUsuario = "";
    public $strNombre = "";
    public $strApPaterno = "";
    public $strApMaterno = "";
    public $dtmFechaNacimiento = "";
    public $intBanderaActivo = 0;
    public $intBanderaPassCambiada = 0;
    public $intBanderaSesionStatus = 0;
    public $intIntentosLogin = 0;
    public $dtmUltimaSesion = "";
    public $dtmFechaIncorporacion = "";
    public $dtmFechaBaja = "";
    public $dtmFechaVigencia = "";
    public $strGenero = "";

      /**
     * Se ejecuta en la construcción del objeto
     */
    public function __construct(){
        try{
            $this->Utils = new Utils();
        }
        catch(Exception $ex){
            console.log($ex);
        }
    }

    /**
     * Obtiene una instancia de sí mismo
     */
    public function getSelfInstance() {
        $Usuario = new Usuario();
        return $Usuario;
    }

    /**
     * 
     */
    public function obtenerDatosUsuario(){
        try{
            /**
             * TODO Obtiene los datos de un usuario identificado por su ID
             */
        }
        catch(Exception $ex){

        }
    }

    public function validarUsuario(){
        try{
            /**
             * TODO Valida que las credenciales de usuario sean correctas de acuerdo al tipo de usuario.
             * Para esto se debe validar en las tablas de subscripción administradores e instructores.
             */
        }
        catch(Exception $ex){

        }
    }

    public function actualizarDatosUsuario(){
        try{

        }
        catch(Exception $ex){
            
        }
    }
}
?>