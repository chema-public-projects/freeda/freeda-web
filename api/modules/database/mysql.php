<?php
$modules_path = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;

class Mysql {
    #PARÁMETROS DE CONEXIÓN
    protected $server_name  = "";
    protected $user_name    = "";
    protected $password     = "";
    protected $db_name      = "";

    /**
     * @override
     * description: Realiza la configuración inicial del objeto
     * returns: none
     */
    public function __construct(){
        $this->server_name  = "localhost";
        $this->user_name    = "root";
        $this->password     = "";
        $this->db_name      = "sgac_1.2";
    }

    /**
     * Establece una conexión con la base de datos del sistema
     * @return:     El objeto de conexión a la base de datos
     */
    public function getDBConnection(){
        try{
            $connection = new PDO("mysql:host={$this->server_name};dbname={$this->db_name}", $this->user_name, $this->password);
            $connection->exec("SET NAMES utf8");//ajuste del charset
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//habilita el catch de las excepciones
            return $connection;
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Establece una conexión con el servidor
     * @return  CONNECTION  Una conexión con el servidor
     */
    public function getServerConnection(){
        try{
            $serverConnection = new PDO("mysql:host={$this->server_name}", $this->user_name, $this->password);
            $serverConnection->exec("SET NAMES utf8");//ajuste del charset
            $serverConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $serverConnection;
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * TODO
     * Permite crear una base de datos en el servidor espcificado en los parámetros de conexión globales de esta clase
     * @param strDBName Nombre de la base de datos a crear
     * @return None
     */
    public function createDatabase($strDBName){
        try{
            /**
             * TODO
             */
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Provee un mecanismo para realizar consultas SQL en las que se obtienen filas de datos
     * @return JSON         Las filas obtenidas en un objeto de notación javascript (JSON) junto con un nodo que indica
     * la cantidad de resultados obtenidos || un JSON con una bandera de error y el motivo del error en caso de que suceda
     * un error durante la ejecución
     * @param strSQLQuery   La consulta SQL para obtener datos    
     */
    public function getRows($strOrigin, $strFields, $strWhereConstraint){
        try{
            $strWhereConstraint = $strWhereConstraint ? $strWhereConstraint : "";//Si fuera algún valor nulo o vacío se utilizará una cadena vacía por defecto
            
            if( !$strFields || !$strOrigin )//si no llegan estos valores se retorna un error
                throw new Exception("Parámetros incompletos");

            $strSQLQuery = "";

            if(!$strWhereConstraint)
                $strSQLQuery = "SELECT {$strFields} FROM {$strOrigin}";//Consulta SQL Select armada
            else
                $strSQLQuery = "SELECT {$strFields} FROM {$strOrigin} WHERE {$strWhereConstraint}";//Consulta SQL Select armada

            $connection = $this->getDBConnection();
            $stmt = $connection->prepare($strSQLQuery);
            $stmt->execute();
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);//configura el ajuste de resultados para que sean parseados como un array
            $rows = $stmt->fetchAll();//ajusta los resultados obtenidos
            $connection = null;//cierra la conexión
            if( $rows ? true : false ){
                return $rows;
            }
            else{
                return false;
            }
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Permite realizar consultas SQL para insertar datos en la base de datos
     * @param   strTableName    Nombre de la tabla objetivo
     * @param   arrColumns      Array de las columnas. Cada columna debe ser un string.
     * @param   arrValues       Array de valores. Los valores deben coincidir con el orden de las columnas respectivamente.
     * @return  intID           El id del nuevo registro o un cero (0) si no se completa el proceso
     */
    public function insert($strTableName, $arrColumns, $arrValues){
        try{            
            if( !$strTableName || !$arrColumns || !$arrValues)//si no llegan estos valores se retorna un error
                throw new Exception("Parámetros incompletos");

            $strSQLQuery = "INSERT INTO {$strTableName} (";
            //Tamaños de los arrays
            $columnsLength = sizeof($arrColumns);
            $valuesLength = sizeof($arrValues);
            //Arma la parte de las columnas
            for($i = 0; $i < $columnsLength; $i++){
                if($i == $columnsLength - 1){
                    $strSQLQuery .= $arrColumns[$i].") ";
                    break;
                }
                $strSQLQuery .= $arrColumns[$i].", ";
            }
            
            $strSQLQuery .= " VALUES (";
            //Arma la sección de los valores
            for( $i = 0; $i < $valuesLength; $i++ ){
                if( $i == $valuesLength - 1 ){
                    $strSQLQuery .= " '{$arrValues[$i]}') ";
                    break;
                }
                $strSQLQuery .= " '{$arrValues[$i]}', ";
            }
            
            $connection = $this->getDBConnection();
            $connection->exec($strSQLQuery);
            $intLastID = $connection->lastInsertId();

            if($intLastID > 0)
                return $intLastID;
            else
                return false;
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Permite realizar una consulta SQL para eliminar un registro
     * @param strTableName
     * @param strWhereConstraint
     * @return JSON un objeto JSON con información que permite determinar el estado del resultado de la consulta
     */
    public function logicDelete($strTableName, $strLogicColumn, $strWhere){
        try{
            $result = $this->update($strTableName, [$strLogicColumn => 0], $strWhere);
            return $result;
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * TODO
     */
    public function deleteOnCascade(){
        try{
            /**
             * TODO
             */
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Permite eliminar un registro de una tabla
     * @param strTableName  Nombre de la tabla objetivo
     * @param strWhere      Sentencia condicional lógica
     */
    public function hardDelete($strTableName, $strWhere){
        try{
            $strSQLQuery = "DELETE {$strTableName} WHERE {$strWhere}";            
            $connection = $this->getDBConnection();
            $stmt = $connection->prepare($strSQLQuery);
            $result = $stmt->execute();
            $connection = null;//cierra la conexión

           if($result > 0){
               return true;
           }
           else{
               return false;
           }
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }

    /**
     * Permite realizar una consulta SQL para actualizar los datos de una fila
     * @param strTableName          El nombre de la tabla que se actualizará
     * @param strKeyValuePairs      El segmento SQL que corresponde a la asignación de valores para cada columna
     * @param strWhereConstraint    La cláusula Where de la consulta SQL que evita que se actualice una tabla completa por error
     * @return  JSON    Un objeto en notación JSON que permite determinar el estado de la consulta SQL
     */
    public function update($strTableName, $arrKeyValuePairs, $strWhereConstraint){
        try{
            $allKeys = array_keys($arrKeyValuePairs);
            $allValues = array_values($arrKeyValuePairs);
            $strSQLQuery = "UPDATE {$strTableName} SET ";
            $arrKeyValuePairsLength = sizeof($arrKeyValuePairs);

           for($i = 0; $i < $arrKeyValuePairsLength; $i++){
               if($i == $arrKeyValuePairsLength - 1){
                $strSQLQuery .= "{$allKeys[$i]} = '{$allValues[$i]}' ";
                break;
               }
            $strSQLQuery .= "{$allKeys[$i]} = '{$allValues[$i]}', ";
           }

           $strSQLQuery .= "WHERE {$strWhereConstraint}";
           $connection = $this->getDBConnection();
           $stmt = $connection->prepare($strSQLQuery);
           $result = $stmt->execute();
           $connection = null;//cierra la conexión

           if($result > 0){
               return true;
           }
           else{
               return false;
           }
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
        catch(PDOException $ex){
            throw new Exception($ex->getMessage());
        }
    }
}
?>