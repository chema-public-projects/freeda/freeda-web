<?php
    class ActividadesComplementarias{
        public function __constructor(){

        }

        public function getActividades(){
            $middleware = function ($request, $response, $next){
                $response->getBody()->write(json_encode(["yep" => "value 2"]));
                /*
                    Este middleware se ejecuta primero
                    Esta linea devuelve el control al invocador por un momento y después continúa ejecutandose el código de esta función
                    $response = $next($request, $response);
                */
                return $response;
            };

            return $middleware;
        }
    }
?>