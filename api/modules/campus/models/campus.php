<?php
$modules_path = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
require $modules_path.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'utils.php';
require $modules_path.DIRECTORY_SEPARATOR.'database'.DIRECTORY_SEPARATOR.'mysql.php';

class Campus{
    private $Utils;

    public $intIdCamp = 0;
    public $strNombreCampus = "";
    public $strDescripcion = "";
    public $dcmLatitud = 0.0;
    public $dcmLongitud = 0.0;
    public $intZoom = 0;
    public $strUrlMapa = "";
    public $intBanderaActivo = 0;

    /**
     * Se ejecuta en la construcción del objeto
     */
    public function __construct(){
        try{
            $this->Utils = new Utils();
        }
        catch(Exception $ex){
            console.log($ex);
        }
    }

    /**
     * Obtiene una instancia de sí mismo
     */
    public function getSelfInstance() {
        $Campus = new Campus();
        return $Campus;
    }

    /**
     * Obtiene todos los campus activos
     */
    public function obtenerTodosCampus(){
        try{
            $db = new Mysql();
            $rows = $db->getRows("campus", "*", "");
            
            return $this->Utils->Messages->getResponseStructure(
                "OK",
                $this->Utils->Http->getHttpCodes()['OK'],
                "Operación exitosa",
                "Se han obtenido los campus correctamente",
                sizeof($rows),
                $rows
            );
        }
        catch(Exception $ex){
            return ($this->Utils->Messages->getResponseStructure(
                "INTERNAL_SERVER_ERROR",
                $this->Utils->Http->getHttpCodes()['INTERNAL_SERVER_ERROR'],
                "Ha ocurrido un error",
                "La operación terminó inesperamente a causa de un error. {$ex}",
                0,
                null
            ));
        }
    }

    /**
     * 
     */
    public function registrarCampus(){
        try{
            $db = new Mysql();
            $arrColumnas = [
                            "strNombreCampus", 
                            "strDescripcion", 
                            "dcmLatitud", 
                            "dcmLongitud", 
                            "intZoom", 
                            "strUrlMapa", 
                            "intBanderaActivo"
                            ];
            $arrValores = [
                            $this->strNombreCampus, 
                            $this->strDescripcion, 
                            $this->dcmLatitud, 
                            $this->dcmLongitud, 
                            $this->intZoom, 
                            $this->strUrlMapa, 
                            $this->intBanderaActivo
                            ];
            $db->insert("campus", $arrColumnas, $arrValores);
        }
        catch(Exception $ex){
            echo $ex;
        }
    }

    public function modificarCampus(){
        try{
            $db = new Mysql();
            $arrCampoValor = [
                                "strNombreCampus" => $this->strNombreCampus, 
                                "strDescripcion" => $this->strDescripcion, 
                                "dcmLatitud" => $this->dcmLatitud, 
                                "dcmLongitud" => $this->dcmLongitud, 
                                "intZoom" => $this->intZoom, 
                                "strUrlMapa" => $this->strUrlMapa, 
                                "intBanderaActivo" => $this->intBanderaActivo
                            ];
            $db->update("campus", $arrCampoValor, "intIdCamp = '{$this->intIdCamp}'");
        }
        catch(Exception $ex){
            echo $ex->getMessage();
        }
    }
}

$campus = new Campus();
$campus->obtenerTodosCampus();

/*
$campus->intIdCamp = 1;
$campus->strNombreCampus = "LONDRES";
$campus->strDescripcion = "Campus en la zona de INGLATERRA";
$campus->dcmLatitud = "57.57485";
$campus->dcmLongitud = "78.3569763";
$campus->intZoom = "54";
$campus->strUrlMapa = "maps.google.com";*/
//$campus->intBanderaActivo = 1;
//$campus->registrarCampus();
//$campus->modificarCampus();
?>