<?php
session_start();
if(isset($_SESSION['TIPO_USUARIO'])){
  if($_SESSION['TIPO_USUARIO'] == "ADMINISTRADOR"){
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <?php require_once "partial_views/alumnos/head.php";?>

    <body class="nav-md">
      <!--loading-->
      <div id="loading">
        <div id="pantallaCompletaTransparente">
         <div class="loader" style="width:90px; margin: 300px auto;"></div>
       </div>
     </div>
     <div id="modalResultado"></div>
     <!--end loading-->
     
     <div class="container body"><!--container-->
      <div class="main_container"><!--main container-->
        <?php require_once "partial_views/menu_fixed.php"; ?>
        <?php require_once "partial_views/top_navigation.php"; ?>

        
        <div class="right_col" role="main"><!-- page content -->
            <div class="clearfix"></div>

            <div class="row"><!--row-->
              <div class="col-md-12 col-sm-12 col-xs-12"><!--col--> 
                <div class="x_panel"><!--x_panel-->

                  <div class="x_title">
                    <h2>Alumnos  <small>Activos</small></h2> 
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content"><!--x_content-->
                    <table id="tbTablaAlumnos" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th style="min-width: 150px">Nombre</th>
                          <th style="min-width: 150px">Apellido Paterno</th>
                          <th style="min-width: 150px">Apellido Materno</th>
                          <th style="min-width: 150px">No. Control</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Walton</td>
                          <td>Martinez</td>
                          <td>Edinburgh</td>
                          <td>146w0898</td>
                        </tr>
                        <tr>
                          <td>Nestor</td>
                          <td>Meza</td>
                          <td>Edinburgh</td>
                          <td>146w0663</td>
                        </tr>
                        <tr>
                          <td>Miller</td>
                          <td>Saints</td>
                          <td>Edinburgh</td>
                          <td>146w0370</td>
                        </tr>
                      </tbody>
                    </table>
                  </div> <!--end x_content-->
                </div> <!--end x_panel-->
              </div> <!--end col-->
            </div> <!--end row-->
        </div><!-- /page content -->        
        <?php require_once "partial_views/footer_content.php"; ?>
      </div><!--main container-->
    </div><!--container-->

    <?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/global.js"></script><!-- jquery.inputmask -->
    <script src="../main/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../main/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Switchery -->
    <script src="../main/vendors/switchery/dist/switchery.min.js"></script>

    <!-- Datatables -->
    <script src="../main/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../main/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../main/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../main/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../main/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../main/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../main/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../main/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../main/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../main/vendors/pdfmake/build/vfs_fonts.js"></script>


   
    <script src="js/alumnos_activos.js"></script>
    
    <script type="text/javascript">
      $(document).ready(function(){
        $("#tbTablaAlumnos").DataTable({
          dom: "Bfrtip",
                buttons: [{
                    extend: "copy",
                    className: "btn-sm"
                }, {
                    extend: "csv",
                    className: "btn-sm"
                }, {
                    extend: "pdf",
                    className: "btn-sm"
                }, {
                    extend: "excel",
                    className: "btn-sm"
                }, {
                    extend: "print",
                    className: "btn-sm"
                }],
                deferRender: !0,
            scrollY: 380,
            scrollCollapse: !0,
            scroller: !0
                
        });
      })
      
      
    </script>

  </body>
  </html>

  <?php
    }//check for admin session
    else{
      switch ($_SESSION['TIPO_USUARIO']) {
        case 'INSTRUCTOR':
        echo '<script> window.location.href = "../instructores/."</script>';
        break;
        case 'ALUMNO':
        echo '<script> window.location.href = "../alumnos/."</script>';
        break;
      }
    }
} //check for session
else {
  echo '<script> window.location.href = "../../admin"</script>';
}
?>
