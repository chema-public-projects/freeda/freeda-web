<?php
session_start();
if(isset($_SESSION['TIPO_USUARIO'])){
    if($_SESSION['TIPO_USUARIO'] == "ADMINISTRADOR"){
?>

<!DOCTYPE html>
<html lang="en">
<?php require_once "partial_views/actividades/head.php";?>

  <body class="nav-md">
      <div id="loading">
    		<div id="pantallaCompletaTransparente">
    			<div class="loader" style="width:90px; margin: 300px auto;"></div>
    		</div>
    	</div>
        <div id="modalResultado"></div>
        <div class="container body">
            <div class="main_container">
<?php require_once "partial_views/menu_fixed.php"; ?>
<?php require_once "partial_views/top_navigation.php"; ?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">

<?php require_once "partial_views/actividades/page_content.php"; ?>

  </div>
</div>
<!-- /page content -->
<?php require_once "partial_views/footer_content.php"; ?>
            </div>
        </div>

<?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/global.js">
    </script><!-- jquery.inputmask -->
    <script src="../main/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../main/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- Switchery -->
    <script src="../main/vendors/switchery/dist/switchery.min.js"></script>

    <script src="js/actividades_activas.js"></script>
    <script src="js/actividades_inactivas.js"></script>
    <script src="js/crear_actividad.js"></script>
    

  </body>
</html>

<?php
    }//check for admin session
    else{
        switch ($_SESSION['TIPO_USUARIO']) {
            case 'INSTRUCTOR':
                    echo '<script> window.location.href = "../instructores/."</script>';
                break;
            case 'ALUMNO':
                echo '<script> window.location.href = "../alumnos/."</script>';
                break;
        }
    }
} //check for session
else {
    echo '<script> window.location.href = "../../admin"</script>';
}
?>
