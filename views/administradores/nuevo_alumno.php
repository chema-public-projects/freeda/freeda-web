<?php
session_start();
if(isset($_SESSION['TIPO_USUARIO'])){
  if($_SESSION['TIPO_USUARIO'] == "ADMINISTRADOR"){
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <?php require_once "partial_views/alumnos/head.php";?>

    <body class="nav-md">
      <div id="loading">
        <div id="pantallaCompletaTransparente">
         <div class="loader" style="width:90px; margin: 300px auto;"></div>
       </div>
     </div>
     <div id="modalResultado"></div>
     <div class="container body">
      <div class="main_container">
        <?php require_once "partial_views/menu_fixed.php"; ?>
        <?php require_once "partial_views/top_navigation.php"; ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row"><!--row-->
              <div class="col-md-12 col-sm-12 col-xs-12"><!--col--> 
                <div class="x_panel"><!--x_panel-->

                  <div class="x_title">
                    <h2>Registrar Nuevo Alumno <small>Todos los campos son obligatorios</small></h2>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content"><!--x_content-->
                    <br />

                    <!---form-->
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">No. Control<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre(s)<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Apellido Paterno<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Apellido Materno<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Dirección</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <!--textarea id="txtDescripcion" required="required" name="message"  style="margin: 0px -14.5px 0px 0px; height: 88px; width: 499px;" ></textarea-->
                          <textarea class="form-control" rows="5" id="txtDescripcion"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Genero<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Carrera<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Campus<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Semestre<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Teléfono<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Limitaciones<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status <span class="required"></span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" checked />  No Disponible /Disponible
                            </label>
                          </div>
                        </div>
                      </div> 

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Estado <span class="required"></span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="">
                            <label>
                              <input type="checkbox" class="js-switch" checked id="chkEstado"/>  Inactiva / Activa
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancelar</button>
                          <button class="btn btn-primary" type="reset">Cancel Limpiarar</button>
                          <button class="btn btn-primary" type="reset">Registrar Alumnos</button>
                        </div>
                      </div>

                    </form> <!---end form--> 
                  </div> <!--end x_content-->
                </div> <!--end x_panel-->
              </div> <!--end col-->
            </div> <!--end row-->



          </div>
        </div>
        <!-- /page content -->
        <?php require_once "partial_views/footer_content.php"; ?>
      </div>
    </div>

    <?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/global.js">
    </script><!-- jquery.inputmask -->
    <script src="../main/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../main/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- Switchery -->
    <script src="../main/vendors/switchery/dist/switchery.min.js"></script>

    <script src="js/actividades_activas.js"></script>
    <script src="js/actividades_inactivas.js"></script>
    <script src="js/crear_actividad.js"></script>

  </body>
  </html>

  <?php
    }//check for admin session
    else{
      switch ($_SESSION['TIPO_USUARIO']) {
        case 'INSTRUCTOR':
        echo '<script> window.location.href = "../instructores/."</script>';
        break;
        case 'ALUMNO':
        echo '<script> window.location.href = "../alumnos/."</script>';
        break;
      }
    }
} //check for session
else {
  echo '<script> window.location.href = "../../admin"</script>';
}
?>
