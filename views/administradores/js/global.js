$(".btnLogout").click(function(){
    logout();
});

//Deshabilita la tecla ENTER
$(document).keypress(
    function(event){
     if (event.which == '13') {
         console.log(msg.debug)
        event.preventDefault();
      }
});

function logout(){
    try{
        crearLoadingTransparente();
        obj = {query: "logout"}
        var url = "../../controllers/AdministradorController";
        $.ajax({
          type: 'POST',
          url: url,
          data: obj,
          success: function(respuesta){//Respuesta es lo que pone PHP en pantalla, prácticamente es traer al PHP si la consulta fue successfuly
              var error = contiene(respuesta, "error");
              var falso = contiene(respuesta, "false");
              var ok = contiene(respuesta, "ok");
              if(ok || error || falso){
                  if(falso)
                    mostrarModalInfoResultDismisable("WARNING", msg.error.generic, msg.error.noLogout);
                  if(error)
                    mostrarModalInfoResultDismisable("ERROR", msg.error.generic, msg.exception.internalError);
                  if(ok)
                      window.location.href = path.p_1.loginAdmin;
              }else{
                  mostrarModalInfoResultDismisable("ERROR", msg.error.generic, msg.exception.noJson);
                  $("#mensaje").html(msg.exception.noJson);
                  console.log(msg.debug.unexpectedResponse);
              }
              quitarLoading();
          },//END SUCCESS
          error: function(jqXHR, textStatus, errorThrown){
            if(errorThrown == "Not Found"){
                mostrarModalInfoResultDismisable("ERROR", msg.error.generic, msg.exception.notFound);
                console.log(msg.debug.ajaxRequestNotFound);
            }else{
                mostrarModalInfoResultDismisable("ERROR", msg.error.generic, msg.exception.internalError);
              console.log(msg.debug.ajaxException + errorThrown);
            }
            quitarLoading();
          }
        });//AJAX
    }
    catch(err){
      $("#mensaje").html(msg.error.generic);
      console.log(msg.debug.genericException + err.message);
      quitarLoading();
    }
}
