$(document).ready(function(){
    $("#footerText").html(labels.footerText);
    console.log(msg.debug.domReady);
     quitarLoading();//quitar el loader

    construirTablasActivos();
    construirTablasInactivos();
  });

  function construirTablasActivos(){

        $("#tbTablaInstructores_activos").DataTable({
          dom: "Bfrtip",
                buttons: [{
                    extend: "copy",
                    className: "btn-sm"
                }, {
                    extend: "csv",
                    className: "btn-sm"
                }, {
                    extend: "pdf",
                    className: "btn-sm"
                }, {
                    extend: "excel",
                    className: "btn-sm"
                }, {
                    extend: "print",
                    className: "btn-sm"
                }],
                responsive: !0       
        });
  }
   
   function construirTablasInactivos(){
   	$("#tbTablaInstructores_Inactivos").DataTable({
          dom: "Bfrtip",
                buttons: [{
                    extend: "copy",
                    className: "btn-sm"
                }, {
                    extend: "csv",
                    className: "btn-sm"
                }, {
                    extend: "pdf",
                    className: "btn-sm"
                }, {
                    extend: "excel",
                    className: "btn-sm"
                }, {
                    extend: "print",
                    className: "btn-sm"
                }],
                responsive: !0       
        });
   } 
  
      
    