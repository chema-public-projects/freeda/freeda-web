$(document).ready(function(){
  $("#footerText").html(labels.footerText);
  console.log(msg.debug.domReady);
   quitarLoading();//quitar el loader
});

function tryLogin(obj){
  try{
    if(validarObjeto(obj)){
      crearLoadingTransparente();
      obj.query = "login";
      var url = "controllers/AdministradorController";
      $.ajax({
        type: 'GET',
        url: url,
        data: obj,
        success: function(respuesta){//Respuesta es lo que pone PHP en pantalla, prácticamente es traer al PHP si la consulta fue successfuly
            var error = contiene(respuesta, "error");
            var falso = contiene(respuesta, "false");
            var ok = contiene(respuesta, "ok");
            if(ok || error || falso){
                if(falso)
                    $("#mensaje").html(msg.error.noLogin);
                if(error)
                    $("#mensaje").html(msg.exception.internalError);
                if(ok)
                    window.location.href = path.p_1.inicioAdmin;
            }else{
                $("#mensaje").html(msg.exception.noJson);
                console.log(msg.debug.unexpectedResponse);
            }
            quitarLoading();
        },//END SUCCESS
        error: function(jqXHR, textStatus, errorThrown){
          if(errorThrown == "Not Found"){
              $("#mensaje").html(msg.exception.notFound);
              console.log(msg.debug.ajaxRequestNotFound);
          }else{
            $("#mensaje").html(msg.exception.internalError);
            console.log(msg.debug.ajaxException + errorThrown);
          }
          quitarLoading();
        }
      });//AJAX
    }//end if
    else{
      $("#mensaje").html(msg.error.loginEmpty);
      quitarLoading();
    }//end else
  }
  catch(err){
    $("#mensaje").html(msg.error.generic);
    console.log(msg.debug.genericException + err.message);
    quitarLoading();
  }
}

$("#btnLogin").click(function(){
  obj = {
    usuario: $("#usuario").val(),
    clave: $("#clave").val()
  }
  tryLogin(obj);
});
