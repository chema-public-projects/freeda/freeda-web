<?php
session_start();
if(isset($_SESSION['TIPO_USUARIO'])){
    if($_SESSION['TIPO_USUARIO'] == "ADMINISTRADOR"){
?>

<!DOCTYPE html>
<html lang="en">
<?php require_once "partial_views/alumnos/head.php";?>

  <body class="nav-md">
      <div id="loading">
    		<div id="pantallaCompletaTransparente">
    			<div class="loader" style="width:90px; margin: 300px auto;"></div>
    		</div>
    	</div>
        <div id="modalResultado"></div>
        <div class="container body">
            <div class="main_container">
<?php require_once "partial_views/menu_fixed.php"; ?>
<?php require_once "partial_views/top_navigation.php"; ?>
<!-- page content -->


<div class="right_col" role="main">
  <div class="">

<?php require_once "partial_views/alumnos/page_content.php"; ?>

  </div>
</div>
<!-- /page content -->
<?php require_once "partial_views/footer_content.php"; ?>
            </div>
        </div>

<?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/global.js">
    </script><!-- jquery.inputmask -->
    <script src="../main/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../main/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- Switchery -->
    <script src="../main/vendors/switchery/dist/switchery.min.js"></script>

     <!-- Datatables -->
    <script src="../main/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../main/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../main/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../main/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../main/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../main/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../main/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../main/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../main/vendors/jszip/dist/jszip.min.js"></script>
    <script src="../main/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../main/vendors/pdfmake/build/vfs_fonts.js"></script>


    <script src="js/crear_alumno.js"></script>
    <script src="js/ver_alumnos_activos.js"></script>
    <script src="js/ver_alumnos_inactivos.js"></script>
    <script src="js/carga_masiva_alumnos.js"></script>



    <script type="text/javascript" src="../main/js/location_picker.js"></script>
    

  </body>
</html>

<?php
    }//check for admin session
    else{
        switch ($_SESSION['TIPO_USUARIO']) {
            case 'INSTRUCTOR':
                    echo '<script> window.location.href = "../instructores/."</script>';
                break;
            case 'ALUMNO':
                echo '<script> window.location.href = "../alumnos/."</script>';
                break;
        }
    }
} //check for session
else {
    echo '<script> window.location.href = "../../admin"</script>';
}
?>
