<?php
session_start();
if(isset($_SESSION['TIPO_USUARIO'])){
    if($_SESSION['TIPO_USUARIO'] == "ADMINISTRADOR"){
?>

<!DOCTYPE html>
<html lang="en">
<?php require_once "partial_views/alumnos/head.php";?>

  <body class="nav-md">
      <div id="loading">
    		<div id="pantallaCompletaTransparente">
    			<div class="loader" style="width:90px; margin: 300px auto;"></div>
    		</div>
    	</div>
        <div id="modalResultado"></div>
        <div class="container body">
            <div class="main_container">
<?php require_once "partial_views/menu_fixed.php"; ?>
<?php require_once "partial_views/top_navigation.php"; ?>

    <div class="right_col" role="main"><!-- page content -->
            <div class="clearfix"></div>

            <div class="row"><!--row-->
              <div class="col-md-12 col-sm-12 col-xs-12"><!--col--> 
                <div class="x_panel"><!--x_panel-->

                  <div class="x_title">
                    <h2> Carga Masiva<small>Alumnos</small></h2> 
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content"><!--x_content-->
                    <div class="row"><!--row-->
                      <div class="col-md-12"><!--col-->
                        <h2>Selecciona un archivo excel</h2>
                          <div class="btn-group">
                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                            <br>
                            <button type="button" class="btn btn-success btn-md">Subir</button>
                           </div>
                      </div><!-- end col-->
                    </div><!-- end row-->
                    <br>
                    <div class="row" style="border-top: solid 1px #999; padding: 7px;"><!--row-->
                      <div class="col-md-4" style="border-right: solid 1px #999;">  <!--col-->   
                         <p>Avance</p>
                          <input class="knob" data-width="100" data-height="120" data-min="-100" data-displayPrevious=true data-fgColor="#26B99A" value="44">
                      </div><!--end col-->
                      <div class="col-md-8">
                        <div class="form-group"><!--from-group-->
                          <div class="row">
                            <div class="col-md-4"><label>inserciones Correctas</label></div>
                            <div class="col-md-8"><label>0</label></div>
                          </div>
                        </div><!--end form-group-->
                        <div class="form-group">
                          <div class="row">
                            <div class="col-md-4"><label>Errores</label></div>
                            <div class="col-md-8">0</div>
                          </div>
                        </div>
                      </div>
                     </div><!--end row-->

                    </div> <!--end x_content-->
                </div> <!--end x_panel-->
              </div> <!--end col-->
            </div> <!--end row-->
        </div><!-- /page content --> 
<!-- /page content -->
<?php require_once "partial_views/footer_content.php"; ?>
            </div>
        </div>

<?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/global.js">
    </script><!-- jquery.inputmask -->
    <script src="../main/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jQuery Knob -->
    <script src="../main/vendors/jquery-knob/dist/jquery.knob.min.js"></script>
     <!-- Switchery -->
    <script src="../main/vendors/switchery/dist/switchery.min.js"></script>

    <script src="js/actividades_activas.js"></script>
   
   

  </body>
</html>

<?php
    }//check for admin session
    else{
        switch ($_SESSION['TIPO_USUARIO']) {
            case 'INSTRUCTOR':
                    echo '<script> window.location.href = "../instructores/."</script>';
                break;
            case 'ALUMNO':
                echo '<script> window.location.href = "../alumnos/."</script>';
                break;
        }
    }
} //check for session
else {
    echo '<script> window.location.href = "../../admin"</script>';
}
?>
