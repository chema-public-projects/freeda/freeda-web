<!-- jQuery -->
<script src="../main/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../main/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../main/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../main/vendors/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script src="../main/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="../main/build/js/custom.min.js"></script>
