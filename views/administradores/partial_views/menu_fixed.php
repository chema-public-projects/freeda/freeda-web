<!--menu fixed -->
<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="." class="site_title"><i class="fa fa-graduation-cap" style="border: none"></i> <span>SGAC</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="../main/img/users/administrador.png" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Bienvenido</span>
        <h2>Nombre del administrador</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Opciones</h3>
        <ul class="nav side-menu">
          <li><a href="."><i class="fa fa-home"></i> Inicio <span class="fa fa-circle"></span></a></li>
          <li><a  href="aviso"><i class="fa fa-envelope-o"></i> Avisos <span class="fa fa-circle"></span></a>
          </li>
          <li><a href="actividades"><i class="fa  fa-bicycle"></i> Actividades <span class="fa fa-circle"></span></a>
           </li>
          <li><a href="lugares"><i class="fa fa-map-marker"></i> Lugares <span class="fa fa-circle"></span></a>
          </li>
          <li><a><i class="fa fa-graduation-cap"></i> Alumnos <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="nuevo_alumno">Crear nuevo alumno</a></li>
              <li><a href="alumnos_activos">Alumnos activos</a></li>
              <li><a href="alumnos_inactivos">Alumnos inactivos</a></li>
              <li><a href="carga_masiva_alumnos">Carga masiva alumnos</a></li>
            </ul>
          </li>
          <li><a href="instructores"><i class="fa  fa-bicycle"></i> Instructores <span class="fa fa-circle"></span></a>
         </li>  
          <li><a><i class="fa fa-file-pdf-o"></i> Reposiciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="horarios">Carta liberación</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-upload"></i> Publicaciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="Convocatorias">Convocatorias</a></li>
              <li><a href="horarios">Horarios</a></li>
            </ul>
          </li>
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Administración de sistema" href="administracion">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Enviar un comentario">
        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Ayuda">
        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Salir" href="#"  class="btnLogout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
<!--menu fixed -->
