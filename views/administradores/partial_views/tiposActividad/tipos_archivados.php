<div class="x_panel"><!--x_panel-->

                  <div class="x_title">
                    <h2>Todos lo Tipos de Actividad  <small>Inactivos</small></h2> 
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content"><!--x_content-->
                    <table id="tbTipoActividad" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th style="min-width: 50px">ID</th>
                          <th style="min-width: 350px">Nombre</th>
                          <th style="min-width: 100px"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Cultural</td>
                          <td><button type='button' class='btn btn-round btn-success btn-xs'>Reactivar</button></td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Deportivo</td>
                          <td><button type='button' class='btn btn-round btn-success btn-xs'>Reactivar</button></td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Arte</td>
                          <td><button type='button' class='btn btn-round btn-success btn-xs'>Reactivar</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div> <!--end x_content-->
                </div> <!--end x_panel-->