<!--x_panel-->
    <div class="x_panel">

      <div class="x_title">
        <h2>Crear Nueva Tipo de Actividad <small>Todos los campos son obligatorios</small></h2>
        <div class="clearfix"></div>
      </div>
      <!--x_content-->
      <div class="x_content">
        <br />
        <div><label style="padding-left: 75px;">Un tipo de actividad complementarian es una categoría a la que una actividad pertenece.
        Si desea agregar una nueva categoría ingrese el nombre del tipo y seleccione el botón de confimación</label></div>
        <br>
        
        <!---form-->
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre de la actividad<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <button class="btn btn-primary" type="button">Agregar</button>
      </div>
  </div>
   </form>
   <!---end form-->
  </div>
   <!--end x_content-->
</div>
<!--end x_panel-->