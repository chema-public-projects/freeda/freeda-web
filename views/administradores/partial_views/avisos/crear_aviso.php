       
                <div class="x_panel"><!--x_panel-->
                  <div class="x_title"><!--x_title-->
                    <h2>Crear un Aviso <small>ITSZ</small></h2>
                    <div class="clearfix"></div>

                  </div>
                  <div class="x_content">
                    <br />
                    <form  data-parsley-validate class="form-horizontal form-label-left" onsubmit="return false" id=""><!--from-->
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="checkbox" id="" value="true" > Aviso general
                        </div>
                      </div>

                     <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Actividad<span class="required"></span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" id="">
                            </select>
                          </div>
                     </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Titulo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripcion<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea id="" class="form-control" rows="3" placeholder=""></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Fecha Fin<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" data-inputmask="'mask': '99-99-9999'" id="">
                            <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                          </div>
                      </div>

                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button id="" type="submit" class="btn btn-success">Emitir</button>  
                          </div>
                      </div>
                      
                      </form><!--end from-->
                    </div><!--x-content-->
                  </div><!--x_panel-->

          