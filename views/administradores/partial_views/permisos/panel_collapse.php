<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-align-left"></i> Collapsible / Accordion <small>Sessions</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">

      <!-- start accordion -->
      <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel">
          <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h4 class="panel-title">Super administrador</h4>
          </a>
          <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <button>AGREGAR PERMISO</button>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Vista</th>
                    <th>Crear</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>avisos</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>crear_actividad</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>actividades_activas</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>actividades_inactivas</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="panel">
          <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <h4 class="panel-title">Observador</h4>
          </a>
          <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
              <button>AGREGAR PERMISO</button>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Vista</th>
                    <th>Crear</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>avisos</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>crear_actividad</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>actividades_activas</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                  <tr>
                    <th>actividades_inactivas</th>
                    <td><input type="checkbox" id="chkCrear"/></td>
                    <td><input type="checkbox" id="chkEditar"/></td>
                    <td><input type="checkbox" id="chkEliminar"/></td>
                    <td>
                      <button>Guardar</button>
                      <button>Revocar</button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- end of accordion -->


    </div>
  </div>
</div>
