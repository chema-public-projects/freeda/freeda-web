<div class="clearfix"></div>

<!--row-->
<div class="row">

  <!--col-->
  <div class="col-md-12 col-sm-12 col-xs-12">
    <!--x_panel-->
    <div class="x_panel">

      <div class="x_title">
        <h2>Crear Nueva Actividad <small>Todos los campos son obligatorios</small></h2>
        <div class="clearfix"></div>
      </div>
      <!--x_content-->
      <div class="x_content">
        <br />
        <!---form-->
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre de la actividad<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo<span class="required"></span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select class="form-control" id="cbTipo">
               <option>Cultural</option>
               <option>Deportiva</option>
               <option>Especial</option>
             </select>
           </div>
         </div>

         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Campus<span class="required"></span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control" id="cbTipo">
             <option>Nogales</option>
             <option>Zongolica</option>
             <option>Tehuipango</option>
           </select>
         </div>
       </div>

       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Instructor<span class="required"></span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="form-control" id="cbTipo">
           <option></option>
           <option></option>
           <option></option>
         </select>
       </div>
     </div>

     <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">lugar<span class="required"></span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="form-control" id="cbTipo">
         <option></option>
         <option></option>
         <option></option>
       </select>
     </div>
   </div>

   <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <!--textarea id="txtDescripcion" required="required" name="message"  style="margin: 0px -14.5px 0px 0px; height: 88px; width: 499px;" ></textarea-->
      <textarea class="form-control" rows="5" id="txtDescripcion"></textarea>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Total horas <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input name="" id="numTotalHoras" type="number" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Valor de Créditos <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input name="" id="numTotalHoras" type="number" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cupo <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input name="" id="numTotalHoras" type="number" class="form-control col-md-7 col-xs-12">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Disponibilidad <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="">
         <label>
            <input type="checkbox" class="js-switch" checked />  No Disponible /Disponible
           </label>
         </div>
    </div>
  </div>

  

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Estado <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="">
        <label>
          <input type="checkbox" class="js-switch" checked id="chkEstado"/>  Inactiva / Activa
        </label>
      </div>
    </div>
  </div>

 

  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <button class="btn btn-primary" type="button">Aceptar</button>
      <button class="btn btn-primary" type="reset">Cancelar</button>
    </div>
  </div>
   </form>
   <!---end form-->
  </div>
   <!--end x_content-->
</div>
<!--end x_panel-->
</div>
<!--end col-->
</div>
<!--end row-->