<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
    <div class="x_title">
    <h2><i class="fa fa-bars"></i> Actividades <small>Navega entre las pestañas</small></h2>
    <div class="clearfix"></div>
    </div>
    <div class="x_content">


    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Crear nueva actividad</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Actividades activas</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Actividades inactivas</a>
        </li>
        </ul>
        <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <?php
            require_once "partial_views/actividades/formulario_nueva_actividad.php";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
        <?php
            require_once "partial_views/actividades/tabla_actividades_activas.php";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">

        <?php
            require_once "partial_views/actividades/tabla_actividades_inactivas.php";
            ?>
           
        </div>
        </div>
    </div>

    </div>
</div>
</div>