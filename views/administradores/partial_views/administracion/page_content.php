<!--row-->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <!--dashboard-->
    <div class="dashboard_graph">
      <div class="row x_title">
        <div class="col-md-12">
          <h3>Administrar información de sistema</small></h3>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
      </div>
      <div class="clearfix"></div>
    </div>
    <!--end dashboard-->
  </div>
</div>
<!--row-->
<br/>


<div class="row top_tiles">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
      <div class="count">0</div>
      <h3>Administradores</h3>
      <p><a href="usuarios">Administrar</a></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-comments-o"></i></div>
      <div class="count">0</div>
      <h3>Campus</h3>
      <p><a href="campus">Administrar</a></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
      <div class="count">0</div>
      <h3>Carreras</h3>
      <p><a href="carreras">Administrar</a></p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i></div>
      <div class="count">.</div>
      <h3>Roles y permisos</h3>
      <p><a href="permisos">Administrar</a></p>
    </div>
  </div>
</div>





<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Variables de sistema</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="#">Operaciones temporales</a>
            <p>Operaciones que pueden estar disponibles temporalmente o durante un lapso indeterminado</p>
          </div>
        </article>
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="tipos">Tipos de actividades complementarias</a>
            <p>Categorías de las actividades complementarias</p>
          </div>
        </article>
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="#">Status de actividades complementarias</a>
            <p>Colección de estados en los que se puede encontrar una actividad complementaria</p>
          </div>
        </article>
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="#">Indexar vistas web al sistema</a>
            <p>Le permite al sistema reconocer las vistas web físicas para poder gestionar los accesos y permisos</p>
          </div>
        </article>
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="#">Enlazar campus con carreras</a>
            <p>Permite especificar las carreras que están disponibles en cada campus</p>
          </div>
        </article>
        <article class="media event">
          <a class="pull-left date">
            <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
          </a>
          <div class="media-body">
            <a class="title" href="#">Limitaciones</a>
            <p>Limitaciones que padecen algunos alumnos</p>
          </div>
        </article>
      </div>
    </div>
  </div>
</div>
