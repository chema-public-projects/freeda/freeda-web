 <div class="x_content"><!--x_content-->
                    <br />

                    <!---form-->
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <h2>Selecciona un archivo para subir</h2>
                     <div class="btn-group">
                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                            <br>
                            <div>
                            <div class="col-md-12">
                              <button type="button" class="btn btn-success btn-sm">Subir</button>
                            </div>
                            </div>
                           </div>


                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre de la Convocatoria<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="5" id="txtDescripcion"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Publicar Día<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fecha Vigencia<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 

                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <div class="btn-group">
                       <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button" aria-expanded="false">Default <span class="caret"></span>
                       </button>
                    </div>
                    </div>
                   <br>
                   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         <button class="btn btn-primary" type="reset">Guardar</button>
                        </div>
                      </div>

                    </form> <!---end form--> 
                  </div> <!--end x_content-->