<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
    <div class="x_title">
    <h2><i class="fa fa-bars"></i> Lugares <small>Navega entre las pestañas</small></h2>
    
    
    <div class="clearfix"></div>
    </div>
    <div class="x_content">


    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Registrar nuevo lugar</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Ver Lugares</a>
        </li>
        </ul>
        <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <?php
            require_once "partial_views/lugares/formulario_nuevo_lugar.php";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
        <?php
            require_once "partial_views/lugares/ver_lugares.php";
            ?>
        </div>
        
        </div>
    </div>

    </div>
</div>
</div>