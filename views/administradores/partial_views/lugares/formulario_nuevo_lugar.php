 <div class="clearfix"></div>
 <div class="row"><!--row-->
 
  <div class="col-md-12 col-sm-12 col-xs-12"><!-- col -->
    <div class="x_panel"><!--x_panel-->


      <div class="x_title">
        <h2>Registrar lugar <small>Todos los campos son obligatorios</small></h2>
        <div class="clearfix"></div>
      </div>

      <div class="x_content"><!--x_content-->
        <br />

        <div id="modalResultado"></div>
        <h2><div id="mensaje" style="color: #f00; text-align: center;"></div></h2>


        <form id="frmNuevoLugar" data-parsley-validate class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nombre del lugar <span class="required"></span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="txtNombreLugar" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dirección</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea class="form-control" rows="3" id="txtDireccion"></textarea>
            </div>
          </div>
          

          <div class="ln_solid"></div>
          <h2>Ubicación <small>Busca el lugar y selecciona con doble click</small></h2>  

          <div class="form-group"><!-- form-group -->
            <div class="col-md-3 col-sm-3 col-xs-12"></div>                                
            <div class="col-md-6 col-sm-6 col-xs-12"> <!--col -->

             <form> <!--Formulario Abre-->

               <fieldset class="gllpLatlonPicker">

                <div class="form-group"> 
                  <div class="col-md-9 col-sm-9 col-xs-12" style=" margin-bottom: 5px;">
                    <input type="text" id="" required="required" class="form-control col-md-7 col-xs-12 gllpSearchField">
                  </div> 
                  <div class="control-label col-md-3 col-sm-3 col-xs-12" style="padding:0px;">
                    <button class="btn btn-primary gllpSearchButton" type="button" style="max-height: 50px; width: 100%;">Buscar</button>  
                  </div>
                </div>
                
                <div class="gllpMap col-md-12 col-sm-12 col-xs-12" style="min-height: 300px;">Es necesario el uso de Google maps para obtener las coordenadas del lugar.</div><!--mapa incrustado por el API-->

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12"  id="txtlatitud">Latitud: <span class="required"></span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="txtAltitud" required="required" value="18.8504744" class="form-control col-md-7 col-xs-12  gllpLatitude">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" id="txtlongitud">Longitud: <span class="required"></span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="txtLongitud" required="required" class="form-control col-md-7 col-xs-12 gllpLongitude" value="-97.10363960000001">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Zoom: <span class="required"></span></label>
                  <div class="col-md-5 col-sm-5 col-xs-12">
                    <input type="text" id="txtZoom" required="required" class="form-control col-md-7 col-xs-12 gllpZoom" value="12">
                  </div>
                </div>
              </fieldset>
            </form> <!--Formulario Cierra-->
          </div> <!-- col Cierra-->
          <div class="col-md-3 col-sm-3 col-xs-12"></div> 
        </div><!-- end form-group -->

        <div class="ln_solid"></div>

        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success" id="btnGuardarNuevoLugar">Aceptar</button>
            <button class="btn btn-primary" type="button">Cancelar</button>
          </div>
        </div>


      </div><!-- end x_content-->

    </div><!--end x_panel-->
  </div><!-- end col -->
</div><!--end row-->