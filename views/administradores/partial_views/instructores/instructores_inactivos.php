<div class="row"><!--row-->
    <div class="col-md-12 col-sm-12 col-xs-12"><!--col--> 
      <div class="x_panel"><!--x_panel-->

         <div class="x_title">
           <h2>Instructores  <small>Inactivos</small></h2> 
               <div class="clearfix"></div>
         </div>

                  <div class="x_content"><!--x_content-->
              <table id="tbTablaInstructores_Inactivos" class="table table-striped table-bordered">
                   <thead>
                      <tr>
                        <th style="min-width: 150px">Nombre</th>
                        <th style="min-width: 150px">Apellido Paterno</th>
                        <th style="min-width: 150px">Apellido Materno</th>
                        <th style="min-width: 150px">Direcció</th>
                        <th style="min-width: 150px">Correo</th>
                        <th style="min-width: 150px">Teléfono</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Armando</td>
                        <td>Munguia</td>
                        <td>Linares</td>
                        <td>mendoza</td>
                        <td>dark@gmil</td>
                        <td>1234542</td>
                      </tr>
                      <tr>
                        <td>Rene</td>
                        <td>Martinez</td>
                        <td>Cruz</td>
                        <td>y5t66we63</td>
                        <td>orizaba</td>
                        <td>788877994</td>
                      </tr>
                      <tr>
                        <td>Oscar</td>
                        <td>Vazquez</td>
                        <td>Lara</td>
                        <td>998uyue8ej</td>
                        <td>nogales</td>
                        <td>8890000</td>
                      </tr>
                    </tbody>
               </table>
          </div> <!--end x_content-->
       </div> <!--end x_panel-->
   </div> <!--end col-->
</div> <!--end row-->


    

