<div class="row"><!--row-->
    <div class="col-md-12 col-sm-12 col-xs-12"><!--col--> 
      <div class="x_panel"><!--x_panel-->

         <div class="x_title">
           <h2>Instructores  <small>Activos</small></h2> 
               <div class="clearfix"></div>
         </div>

                  <div class="x_content"><!--x_content-->
              <table id="tbTablaInstructores_activos" class="table table-striped table-bordered">
                   <thead>
                      <tr>
                        <th style="min-width: 150px">Nombre</th>
                        <th style="min-width: 150px">Apellido Paterno</th>
                        <th style="min-width: 150px">Apellido Materno</th>
                        <th style="min-width: 150px">Dirección</th>
                        <th style="min-width: 150px">Corrreo</th>
                        <th style="min-width: 150px">Teléfono</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Armando</td>
                        <td>Munguia</td>
                        <td>Linares</td>
                        <td>Orizaba</td>
                        <td>darkrhius@gmail.com</td>
                        <td>2721176485</td>
                        
                      </tr>
                      <tr>
                        <td>Rene</td>
                        <td>Martinez</td>
                        <td>Cruz</td>
                        <td>Mendoza</td>
                        <td>Saint@gmail.com</td>
                        <td>2721347648</td>
                        
                      </tr>
                      <tr>
                        <td>Oscar</td>
                        <td>Vazquez</td>
                        <td>Lara</td>
                        <td>México</td>
                        <td>Mex@gmail.com</td>
                        <td>27233489</td>
                        
                      </tr>
                    </tbody>
               </table>
          </div> <!--end x_content-->
       </div> <!--end x_panel-->
   </div> <!--end col-->
</div> <!--end row-->


 
  
