<div class="col-md-12 col-sm-12 col-xs-12"><!--col-->
<div class="x_panel"><!--x_panel-->
    <div class="x_title"><!--x_title-->
    <h2><i class="fa fa-bars"></i> Instructores <small>Navega entre las pestañas</small></h2>
    
    
    <div class="clearfix"></div>
    </div><!--x_title-->
    <div class="x_content"><!--x_content-->


    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">

        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Crear Instructor</a>
        </li>

        <li role="presentation" class=""><a href="#tab_content2" id="home-tab" role="profile-ta" data-toggle="tab" aria-expanded="false">Activos</a>
        </li>

        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Inactivos</a>
        </li>
        
        </ul>
        <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <?php
            require_once "partial_views/instructores/crear_instructores.php";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade " id="tab_content2" aria-labelledby="profile-tab">
            <?php
            require_once "partial_views/instructores/instructores_activos.php";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
        <?php
            require_once "partial_views/Instructores/instructores_inactivos.php";
            ?>
        </div>
        
        </div>
    </div>

    </div><!--cx_content-->
</div><!--x-panel-->
</div><!--col-->