$(document).ready(function(){
  $("#footerText").html(labels.footerText);
  console.log(msg.debug.domReady);
   quitarLoading();//quitar el loader
});

$("#login-form").click(function(){
    event.preventDefault();
});

$("#btn_login").click(function(){
    crearLoadingTransparente();
    url = "controllers/AlumnoController";
    usuario = $("#usuario").val();
    clave = $("#clave").val();
    objParametros = {
        'usuario' : usuario,
        'clave' : clave
    };
    if(validarObjeto(objParametros)){
        objParametros.query = "login";
      try{
        $.ajax({
              type: 'POST',
              url: url,
              data: objParametros,
              success: function(respuesta){//Respuesta es lo que pone PHP en pantalla, prácticamente es traer al PHP si la consulta fue successfuly
                  var vacio = contiene(respuesta, "VACIO");
                  var error = contiene(respuesta, "ERROR");
                  var ok = contiene(respuesta, "OK");
                  if(vacio || error){
                      if (vacio)
                          $("#mensaje").html(msg.error.loginEmpty);
                      if(error)
                          $("#mensaje").html(msg.error.noLogin);
                      if(ok)
                          window.location.href = path.p_1.inicioAlumno;
                      quitarLoading();
                  }else{
                    $("#mensaje").html(msg.exception.internalError);
                    console.log(msg.debug.unexpectedResponse);
                    quitarLoading();
                  }
              },//END SUCCESS
              error: function(jqXHR, textStatus, errorThrown){
                if(errorThrown == "Not Found"){
                    $("#mensaje").html(msg.exception.notFound);
                    console.log(msg.debug.ajaxRequestNotFound);
                }else{
                  $("#mensaje").html(msg.exception.internalError);
                  console.log(msg.debug.ajaxException + errorThrown);
                }
                quitarLoading();
              }
          });//END AJAX
        //QuitarLoading();
      }//end try
      catch(err){
        $("#mensaje").html(msg.error.generic);
        console.log(msg.debug.genericException + err.message);
        quitarLoading();
      }//end catch
    }//end if
    else{
      $("#mensaje").html(msg.error.loginEmpty);
      quitarLoading();
    }//end else
	});//end click



///////////////////////////////////////////////////////////////////


        function init_chart_doughnut(){
        if( typeof (Chart) === 'undefined'){ return; }
        console.log('init_chart_doughnut');
        if ($('.canvasDoughnut').length){
        var chart_doughnut_settings = {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
              labels: [
                "Completado",
                "Faltantes"
              ],
              datasets: [{
                data: [20, 80],
                backgroundColor: [
                  "#00A998",
                  "#888"
                ],
                hoverBackgroundColor: [
                  "#FAAA00",
                  "#FAAA00"
                ]
              }]
            },
            options: {
              legend: false,
              responsive: false
            }
          }
          $('.canvasDoughnut').each(function(){
            var chart_element = $(this);
            var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
          });
        }
      }
