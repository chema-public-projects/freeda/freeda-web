<!--menu fixed -->
<div class="col-md-3 left_col menu_fixed">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="." class="site_title"><i class="fa fa-graduation-cap" style="border: none"></i> <span>SGAC</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="../main/img/users/alumno.png" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Numero de control</span>
        <h2>Nombre del alumno</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Opciones</h3>
        <ul class="nav side-menu">
          <li><a href="."><i class="fa fa-home"></i> Inicio <span class="fa fa-circle"></span></a></li>
          <li><a href="avisos"><i class="fa fa-home"></i> Avisos <span class="fa fa-circle"></span></a></li>
          <li><a><i class="fa fa-home"></i> Inscripciones <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="inscripciones?campus=1">Nogales</a></li>
              <li><a href="inscripciones?campus=2">Tehuipango</a></li>
              <li><a href="inscripciones?campus=3">Tequila</a></li>
              <li><a href="inscripciones?campus=4">Tezonapa</a></li>
              <li><a href="inscripciones?campus=5">Zongolica</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-edit"></i> Información <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="horarios">Horarios</a></li>
              <li><a href="convocatorias">Convocatorias</a></li>
            </ul>
          </li>
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Ajustes">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Enviar un comentario">
        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Ayuda">
        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Salir" href="#">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
<!--menu fixed -->
