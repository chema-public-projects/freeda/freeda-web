<!--row-->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <!--dashboard-->
    <div class="dashboard_graph">
      <div class="row x_title">
        <div class="col-md-12">
          <h3>Resumen</small></h3>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
      </div>
      <div class="clearfix"></div>
    </div>
    <!--end dashboard-->
  </div>
</div>
<!--row-->
<br/>



<div class="row">
  <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-flag"></i> Cursando</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
    <?php
        /*$objCtrlActividad = new cls_Ctrl_Actividad();
        $actividadesActivas = $objCtrlActividad->mtdObtenerActividadesAlumnoInscrito($_SESSION['strNoControl']);
        $contIns = 0;
        if($actividadesActivas > 0){
          foreach ($actividadesActivas as $act) {
            $contIns++;*/
    ?>
          <!-- start accordion -->
          <div class="accordion" id="accordion<?php // echo $contIns;?>" role="tablist" aria-multiselectable="true">
            <div class="panel">
              <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php //echo $contIns;?>" aria-expanded="false" aria-controls="collapseOne">
                <h4 class="panel-title"><?php //echo $act['strNombreActividad']; ?></h4>
              </a>
              <div id="collapseOne<?php //echo $contIns;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">
                  <ul>
                    <li>Tipo: <?php //echo $act['strCategoria']; ?></li>
                    <li>Descripción: <?php //echo $act['strDescripcion'];?></li>
                    <li>Valor: <?php //echo $act['intCreditos']." "."créditos";?></li>
                    <!--li>Campus: <?php //echo $act['_intIdCamp '];?></li-->
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- end of accordion -->
    <?php
          //}//End foreach
      //  }//End if
    ?>

        </div>
      </div>
    </div>


            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-check"></i> Acreditadas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
              <?php
                  /*$objCtrlActividad = new cls_Ctrl_Actividad();
                  $actividadesActivas = $objCtrlActividad->mtdObtenerAcreditadas($_SESSION['strNoControl']);
                  $contAcr = 0;

                  if($actividadesActivas){
                    foreach ($actividadesActivas as $act) {
                      $contAcr++;
*/
              ?>
                    <!-- start accordion -->
                    <div class="accordion" id="accordionAc<?php //echo $contAcr;?>" role="tablist" aria-multiselectable="true">
                      <div class="panel">
                        <a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOneAc<?php //echo $contAcr;?>" aria-expanded="false" aria-controls="collapseOne">
                          <h4 class="panel-title"><?php //echo $act['strNombreActividad']; ?></h4>
                        </a>
                        <div id="collapseOneAc<?php //echo $contAcr;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                          <div class="panel-body">
                          	<ul>
                              <li>Tipo: <?php //echo $act['strCategoria']; ?></li>
                              <li>Descripción: <?php //echo $act['strDescripcion'];?></li>
                              <li>Valor: <?php //echo $act['intCreditos']." "."créditos";?></li>
                            </ul>
                            <a target="_blank" href="v1/Alumnos/BoletaAcreditacion?act=<?php //echo $act['intIdAct'].'&al='.$_SESSION['strNoControl'];?>"><button class="btn btn-default" type="button">BOLETA</button></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end of accordion -->
              <?php
                /*    }//End foreach
                  }//ENd if

                    $completado = $contAcr * 20;
                    $faltante = 0;
                    if($completado < 100){
                      $faltante = 100 - $completado;
                    }*/
              ?>
                  </div>
                </div>
              </div>

            <div class="col-md-4 col-sm-12 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2><i class="fa fa-dashboard"></i> Progreso %</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <td>
                        <canvas class="canvasDoughnut" height="120" width="120" style="margin: 15px 0px 0px 0; padding: 3px;"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square" style="color: #00A998;"></i>Completado</p>
                            </td>
                            <td style=""><?php //echo $completado."  %"; ?></td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square " style="color: #999;"></i>Faltantes</p>
                            </td>
                           <td><?php //echo $faltante."%"; ?></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
