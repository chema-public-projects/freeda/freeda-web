<!DOCTYPE html>
<html lang="en">
<?php require_once "partial_views/horarios/head.php"; ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
<?php require_once "partial_views/menu_fixed.php"; ?>
<?php require_once "partial_views/top_navigation.php"; ?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">

<?php require_once "partial_views/horarios/page_content.php"; ?>

  </div>
</div>
<!-- /page content -->
<?php require_once "partial_views/footer_content.php"; ?>
      </div>
    </div>

<?php require_once "partial_views/common_scripts.php"; ?>
    <script src="../main/js/enumeraciones.js"></script>
    <script src="../main/js/validator.js"></script>
    <script src="../main/js/utils.js"></script>
    <script src="js/avisos.js"></script>
  </body>
</html>
