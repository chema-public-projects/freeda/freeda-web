/**
* Determina si el valor del parámetro val tiene una longitud mínima determinada por minLenght
* Retorna false si la longitud es inferior a minLenght o true si es igual o mayor
*/
function minCheck(minLenght, val){
  try{
    return (val.leght >= minLenght) ? true : false;
  }
  catch(err){
    console.log(msg.debug.dataTypeError + err.message);
    return false;
  }
}

/**
* Determina si el valor del parámetro val tiene una longitud máxima determinada por maxLenght
* Retorna false si la longitud es mayor a maxLenght o true si es igual o inferior
*/
function maxCheck(maxLenght, val){
  try{
    return (val.leght <= maxLenght) ? true : false;
  }
  catch(err){
    console.log(msg.debug.dataTypeError + err.message);
    return false;
  }
}

/**
* Determina si el valor del parámetro val tiene una longitud entre minLenght y maxLenght
* Retorna false si la longitud no está en ese rango y true si cumple con el rango
*/
function minMaxCheck(minLenght, maxLenght, val){
  try{
    return (val.leght >= minLenght && val.leght <= maxLenght) ? true : false;
  }
  catch(err){
    console.log(msg.debug.dataTypeError + err.message);
    return false;
  }
}

/**
* Retorna true si un valor es nulo, vacio, indefinido, falso o NaN
* o retorna false si es un valor definido
*/
function isNullOrEmpty(val){
  return (val) ? false : true;
}
/**
* Itera sobre las propiedades de un objeto y determina si son validos
* Si alguno no es valido retorna false o true si todos son correctos
*/
function validarObjeto(obj){
  var result = true;
  $.each(obj, function(i, val){
    if (isNullOrEmpty(val)) {
      result = false;
    }
  });
  return result;
}
