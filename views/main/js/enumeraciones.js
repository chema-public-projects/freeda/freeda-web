var msg =
        {
          ok:
            {
              generic : "Proceso finalizado con éxito",
              create : "Se ha creado un nuevo registro",
              get : "Datos obtenidos correctamente",
              update : "La información ha sido actualizada",
              delete : "Los datos han sido eliminados",
              inscripcion: "Inscripción realizada correctamente"
            },
          error:
            {
              generic : "La operación no se completó",
              create : "No se pudo crear el registro",
              get : "Los datos no se obtuvieron",
              update : "No se pudo actualizar",
              delete : "No se pudo eliminar",
              noLogin: "El usuario o la contraseña son incorrectos",
              loginEmpty: "Uno o ambos campos están vacíos",
              inscripcion: "La inscripción no pudo realizarse correctamente",
              noLogout: "No se pudo cerrar sesión"
            },
          warning:
            {
                delete : "Eliminarás datos ¿deseas continuar de todos modos?",
                confirm : "¿Deseas continuar con la operación?"
            },
          exception:
            {
              generic : "Ha ocurrido un error",
              timeout : "El servidor tardó mucho en responder",
              internalError : "Ha ocurrido un error interno en el servidor",
              database : "Ha ocurrido un error en la base de datos",
              noJson : "La información del servidor no pudo ser leída",
              crash : "El sistema se detuvo por un momento!!!",
              notFound: "Imposible contactar con el servidor"
            },
          debug:
          {
            domReady : "Debug Message: DOM loaded",
            ajaxStopReady : "Debug Message: AJAX STOP event launched",
            noJson: "Debug Message: La respuesta del servidor no pudo ser leída, se esperaba JSON válido",
            timeout: "Debug Message: El tiempo de espera se agotó",
            dataLoaded: "Debug Message: Datos obtenidos - ",
            ajaxRequestNotFound: "Debug Message: No se encontró el recurso de la llamada AJAX",
            ajaxException: "Debug Message: Ocurrió la siguiente excepción AJAX: ",
            dataTypeError: "Debug Message: Error de tipo de dato: ",
            genericException: "Debug Message: Ocurrió el siguiente error - ",
            unexpectedResponse: "Debug Message: La respuesta del servidor no puede ser interpretada",
            blockedKey: "Debug Message: Evento keypress de tecla bloqueado"
          }
        }

var path =
        {
          'p_1': {
              icon_ok : "../main/img/icons/checked.svg",
              icon_error : "../main/img/icons/error.svg",
              icon_warning : "../main/img/icons/warning.svg",
              inicioAlumno: "views/alumnos/.",
              inicioInstructores: "views/instructores/.",
              inicioAdmin: "views/administradores/.",
              loginAdmin: "../../admin"
            },
          'p_2': {
              icon_ok : "",
              icon_error : "",
              icon_warning : ""
            },
          'p_3': {
              icon_ok : "",
              icon_error : "",
              icon_warning : ""
            }
          }
var labels =
        {
          footerText : "Sistema de Gestión de Actividades Complementarias v1.2 | ITSZ 2018"
        }
