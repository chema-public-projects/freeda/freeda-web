strNotifIconPassOk = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;"><a href="InicioAlumnos"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding: 0px 10px 10px 10px; padding-top: 50px; color: #FFF;"> <img src="v1/img/icons/checked.svg" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>Bien</h3><p>La próxima vez usa esta contraseña para ingresar.</p><br><button type="button" class="btn btn-round btn-success">Aceptar</button></div></a> </div>';
strNotifIconPassOkAdmin = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;"><a href="Inicio"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding: 0px 10px 10px 10px; padding-top: 50px; color: #FFF;"> <img src="../img/icons/checked.svg" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>Bien</h3><p>La próxima vez usa esta contraseña para ingresar.</p><br><button type="button" class="btn btn-round btn-success">Aceptar</button></div></a> </div>';

function contiene(cadena1, cadena2){
	resultado = cadena1.indexOf(cadena2);
	if(resultado > -1)
    	return true;
    else
    	return false;
}
function crearLoadingPantallaCompleta (){
	$("div#loading").html('<div id="pantallaCompleta"></div>');
	$("div#pantallaCompleta").html('<div class="loader" style="width:80px; margin: 300px auto;"></div>');
}

function crearLoadingTransparente(){
	$("div#loading").html('<div id="pantallaCompletaTransparente"></div>');
	$("div#pantallaCompletaTransparente").html('<div class="loader" style="width:90px; margin: 300px auto;"></div>');
}

function crearLoadingTransparenteBody(){
	$("body").html('<div id="loading"><div id="pantallaCompletaTransparente"><div class="loader" style="width:90px; margin: 300px auto;"></div></div></div>');
}

function quitarLoading(){
	$("div#loading").html("");
}

function deshabilitarPantalla(){
	$("div#loading").html('<div id="pantallaCompletaTransparente"></div>');
}

function mostrarModalInfoResultLink(tipo, link, mensajePrincipal, mensajeSecundario){
	strLink = link;
	strMensajePrincipal = mensajePrincipal;
	strMensajeSecundario = mensajeSecundario;
	strNotifIconRegistroOkLink = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;"><a href="' + strLink + '"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="'+ path.p_1.icon_ok +'" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" class="btn btn-round btn-success">Aceptar</button></div></a> </div>';
	strNotifIconRegistroErrorLink = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;"><a href="' + strLink + '"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="' + path.p_1.icon_error + '" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" class="btn btn-round btn-success">Aceptar</button></div></a> </div>';
	strNotifIconRegistroWarningLink = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;"><a href="' + strLink + '"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="' + path.p_1.icon_warning + '" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" class="btn btn-round btn-success">Aceptar</button></div></a> </div>';

	strLink = link;
	strMensajePrincipal = mensajePrincipal;
	strMensajeSecundario = mensajeSecundario;
	switch(tipo){
		case "OK":
			$("#modalResultado").html(strNotifIconRegistroOkLink);
			break;
		case "ERROR":
			$("#modalResultado").html(strNotifIconRegistroErrorLink);
			break;
		case "WARNING":
			$("#modalResultado").html(strNotifIconRegistroWarningLink);
			break;
	}
}

function mostrarModalInfoResultDismisable(tipo, mensajePrincipal, mensajeSecundario){
strMensajePrincipal = mensajePrincipal;
strMensajeSecundario = mensajeSecundario;
strNotifIconRegistroOkDimisable = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;" class="modalDismisable"><a href="#"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="' + path.p_1.icon_ok + '" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" id="modalResultButton" tabindex="-1" class="btn btn-round btn-success" onclick="dismissModal()">Aceptar</button></div></a> </div>';
strNotifIconRegistroErrorDimisable = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;" class="modalDismisable"><a href="#"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="' + path.p_1.icon_error + '" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" id="modalResultButton" tabindex="-1" class="btn btn-round btn-success"onclick="dismissModal()">Aceptar</button></div></a> </div>';
strNotifIconRegistroWarningDimisable = ' <div style="position:fixed; padding:0; margin:0; top:0; left:0;  width: 100%; height: 100%; background:rgba(255,255,255,0.5); z-index: 5000;" class="modalDismisable"><a href="#"> <div style="min-width: 500px; min-height: 400px; background-color: #456; position:fixed; left: 50%; top:  50%; margin-left: -250px; margin-top:  -250px;  text-align: center; padding-top: 50px; color: #FFF;"> <img src="' + path.p_1.icon_warning + '" width="150px" height="150px" style="margin: auto;"/><br><br><br><h3>' + strMensajePrincipal + '</h3><h2>' + strMensajeSecundario + '</h2><br><button type="button" id="modalResultButton" tabindex="-1" class="btn btn-round btn-success"onclick="dismissModal()">Aceptar</button></div></a> </div>';

	strMensajePrincipal = mensajePrincipal;
	strMensajeSecundario = mensajeSecundario;
	switch(tipo){
		case "OK":
			$("#modalResultado").html(strNotifIconRegistroOkDimisable);
			$('#modalResultButton').attr("tabindex",-1).focus();//enfoca el boton de aceptar de la modal
			break;
		case "ERROR":
			$("#modalResultado").html(strNotifIconRegistroErrorDimisable);
			$('#modalResultButton').attr("tabindex",-1).focus();
			break;
		case "WARNING":
			$("#modalResultado").html(strNotifIconRegistroWarningDimisable);
			$('#modalResultButton').attr("tabindex",-1).focus();
			break;
	}
}
function dismissModal (){
	$("#modalResultado").html("");
}

function getExeptionMessage(){

}