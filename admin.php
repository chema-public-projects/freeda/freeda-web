<?php
error_reporting(E_ERROR);
  session_start();
    if(!$_SESSION['TIPO_USUARIO']){
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Autentificación</title>
  <link rel="icon" type="image/png" href="views/main/img/icon.png">
  <!--Custom Styles-->
  <link rel="stylesheet" type="text/css" href="views/main/css/global.css">
  <link rel="stylesheet" type="text/css" href="views/main/css/loader.css">
  <link rel="stylesheet" href="views/administradores/css/login.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  <div id="loading">
    <div id="loading">
      <div id="pantallaCompletaTransparente">
        <div class="loader" style="width:90px; margin: 300px auto;"></div>
      </div>
    </div>
  </div>

<div class="container">
  <div class="info">
    <h1>Identifícate</h1><span>Administración de Actividades Complementarias ITSZ</a></span>
    <h3 id="mensaje" style="color: #f00;"></h3>
  </div>
</div>
<div class="form">
  <div class="thumbnail"><img src="views/administradores/img/lock.png"/></div>
  <form class="login-form" id="login-form" method="post" onsubmit="return false;">
    <input type="text" placeholder="USUARIO" style="text-align: center;" id="usuario"/>
    <input type="password" placeholder="CONTRASEÑA" style="text-align: center;" id="clave"/>
    <button id="btnLogin">Ingresar</button>
  </form>
</div>
  <script src='views/main/js/jquery/jquery.js'></script>
  <!--CONTROLLERS-->
  <script src="views/main/js/enumeraciones.js"></script>
  <script src="views/main/js/validator.js"></script>
  <script src="views/main/js/utils.js"></script>
  <script type="text/javascript" src="views/administradores/js/login.js"></script>
</body>
</html>
<?php
  }
    else{
        switch($_SESSION['TIPO_USUARIO']){
          case "ALUMNO":
              echo '<script>window.location="."</script>';
          break;//END CASE
          case "ADMINISTRADOR":
              echo '<script>window.location="views/administradores/."</script>';
          break;//END CASE
          case "INSTRUCTOR":
              echo '<script>window.location="views/instructores/."</script>';
          break;//END CASE
        }//END SWITCH
    }
?>
