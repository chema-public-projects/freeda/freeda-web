<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'MySQL/MySQL.php';

class Status{
    //variables globales
    public $intIdStatus = 0;
    public $strNombreStatus = "";
    public $strAbrevStatus = "";

    //constructor
    public function __construct(){

    }

    public function crearStatus(){
        $db = new mysql();
        try{            
            $sql = "INSERT INTO status (strNombreStatus,
                                        strAbrevStatus) 
                                VALUES ('{$this->strNombreStatus}',
                                        '{$this->strAbrevStatus}')";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Registro realizado correctamente"}';
            else
                return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function obtenerStatus(){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM status";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $status = [];//almacena los lugares obtenidos
                foreach($result as $r){
                    $s = new Status();
                    $s->intIdStatus = $r['intIdStatus'];
                    $s->strNombreStatus = $r['strNombreStatus'];
                    $s->strAbrevStatus = $r['strAbrevStatus'];
                    array_push($status, $s);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($status).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }
}
/*
$status = new Status();
$status->strNombreStatus = "INACTIVA";
$status->strAbrevStatus = "I";
echo $status->crearStatus();
echo $status->obtenerStatus();*/
?>
