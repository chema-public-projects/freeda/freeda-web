<?php
    abstract class Usuario{
        //variables globales
        public $intIdUsuario = 0;
        public $strPass = "";
        public $strNombreUsuario = "";
        public $strNombre = "";
        public $strApPaterno = "";
        public $strApMaterno = "";
        public $dtmFechaNacimiento = "";
        public $intBanderaActivo = 0;
        public $intBanderaPassCambiada = 0;
        public $intBanderaSesionStatus = 0;
        public $intIntentosLogin = 0;
        public $dtmUltimaSesion = "";
        public $dtmFechaIncorporacion = "";
        public $dtmFechaBaja = "";
        public $dtmFechaVigencia = "";
        public $strGenero = "";

        public $listTelefonoModel = [];
        public $listCorreoModel = [];


        //constructor
        public function __construct(){

        }

        abstract protected function login($user, $pass);
        abstract protected function registrarNuevo();
        abstract protected function obtenerPorId($id);
        abstract protected function obtenerTodos();
        abstract protected function actualizar();
        abstract protected function deshabilitar($id);
        abstract protected function habilitar($id);
    }
?>
