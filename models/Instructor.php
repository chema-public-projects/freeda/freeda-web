<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'/MySQL/MySQL.php';
require_once $path_models.'Usuario.php';

class Instructor extends Usuario{
    public $_intIdInst = 0;


    public function login($user, $pass){
        $db = new mysql();
        try{
            $sql = "SELECT * FROM
                        usuarios,
                        instructores
                    WHERE usuarios.strNombreUsuario = '{$user}'
                    AND usuarios.strPass = '{$pass}'
                    AND instructores._intIdInst = usuarios.intIdUsuario";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){
                $instructor = new Instructor();
                $instructor->intIdUsuario = $result[0]['intIdUsuario'];
                $instructor->strPass = $result[0]['strPass'];
                $instructor->strNombreUsuario = $result[0]['strNombreUsuario'];
                $instructor->strNombre = $result[0]['strNombre'];
                $instructor->strApPaterno = $result[0]['strApPaterno'];
                $instructor->strApMaterno = $result[0]['strApMaterno'];
                $instructor->dtmFechaNacimiento = $result[0]['dtmFechaNacimiento'];
                $instructor->intBanderaActivo = $result[0]['intBanderaActivo'];
                $instructor->intBanderaPassCambiada = $result[0]['intBanderaPassCambiada'];
                $instructor->intBanderaSesionStatus = $result[0]['intBanderaSesionStatus'];
                $instructor->intIntentosLogin = $result[0]['intIntentosLogin'];
                $instructor->dtmUltimaSesion = $result[0]['dtmUltimaSesion'];
                $instructor->dtmFechaIncorporacion = $result[0]['dtmFechaIncorporacion'];
                $instructor->dtmFechaBaja = $result[0]['dtmFechaBaja'];
                $instructor->dtmFechaVigencia = $result[0]['dtmFechaVigencia'];
                $instructor->strGenero = $result[0]['strGenero'];
                $instructor->_intIdInst = $instructor->intIdUsuario;
                return '{"status":"ok", "msg":"Los datos del usuario fueron obtenidos correctamente", "data":'.json_encode($instructor).'}';
            }
            else
                return '{"status":"false", "msg":"Usuario o contraseña incorrectos"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function registrarNuevo(){
        $mysql = new mysql();
        try{
            $db = $mysql->getPDOConnection();
            $db->beginTransaction();
            $sql = "INSERT INTO usuarios (strPass,
                                        strNombreUsuario,
                                        strNombre,
                                        strApPaterno,
                                        strApMaterno,
                                        dtmFechaNacimiento,
                                        dtmFechaIncorporacion,
                                        dtmFechaVigencia,
                                        strGenero)
                                VALUES (:strPass,
                                        :strNombreUsuario,
                                        :strNombre,
                                        :strApPaterno,
                                        :strApMaterno,
                                        :dtmFechaNacimiento,
                                        :dtmFechaIncorporacion,
                                        :dtmFechaVigencia,
                                        :strGenero)";
            $statement = $db->prepare($sql);
            $statement->bindParam("strPass", $this->strPass);
            $statement->bindParam("strNombreUsuario", $this->strNombreUsuario);
            $statement->bindParam("strNombre", $this->strNombre);
            $statement->bindParam("strApPaterno", $this->strApPaterno);
            $statement->bindParam("strApMaterno", $this->strApMaterno);
            $statement->bindParam("dtmFechaNacimiento", $this->dtmFechaNacimiento);
            $statement->bindParam("dtmFechaIncorporacion", $this->dtmFechaIncorporacion);
            $statement->bindParam("dtmFechaVigencia", $this->dtmFechaVigencia);
            $statement->bindParam("strGenero", $this->strGenero);

            if($statement->execute() > 0){
                if($statement->rowCount() > 0){
                    $idUsuario = $db->lastInsertId();
                    $sql = "INSERT INTO instructores(_intIdInst) VALUES (:_intIdInst)";
                    $statement_2 = $db->prepare($sql);
                    $statement_2->bindParam("_intIdInst", $idUsuario);
                    if($statement_2->execute() > 0){
                        if($statement_2->rowCount() > 0){
                            $db->commit();
                            return '{"status":"ok", "msg":"Se ha registrado un nuevo instructor"}';
                        }
                    }
                    else{
                        $db->rollback();
                        return '{"status":"false", "msg":"El usuario no se pudo registrar como instructor. Revirtiendo cambios."}';
                    }
                }//end if
                else
                    return '{"status":"false", "msg":"No se pudo registrar el usuario"}';
            }
            else
                return '{"status":"false", "msg":"No se pudo registrar el usuario"}';
        }
        catch(PDOException $ex){
            return '{"status":"error", "msg":"' . $ex->getMessage() . '"}';
        }
        finally{
            $mysql = null;
        }
    }
    public function obtenerPorId($id){

    }
    public function obtenerTodos(){
        try{
            $sql = "SELECT * FROM usuarios, instructores WHERE usuarios.intIdUsuario = instructores._intIdInst AND usuarios.intBanderaActivo = 1";
            $db = new mysql();
            $result = $db->mtdConsultaGenerica($sql);
            if($result){
                $instructores = [];
                foreach($result as $i){
                    $instructor = new Instructor();
                    $instructor->intIdUsuario = $i['intIdUsuario'];
                    $instructor->strPass = $i['strPass'];
                    $instructor->strNombreUsuario = $i['strNombreUsuario'];
                    $instructor->strNombre = $i['strNombre'];
                    $instructor->strApPaterno = $i['strApPaterno'];
                    $instructor->strApMaterno = $i['strApMaterno'];
                    $instructor->dtmFechaNacimiento = $i['dtmFechaNacimiento'];
                    $instructor->intBanderaActivo = $i['intBanderaActivo'];
                    $instructor->intBanderaPassCambiada = $i['intBanderaPassCambiada'];
                    $instructor->intBanderaSesionStatus = $i['intBanderaSesionStatus'];
                    $instructor->intIntentosLogin = $i['intIntentosLogin'];
                    $instructor->dtmUltimaSesion = $i['dtmUltimaSesion'];
                    $instructor->dtmFechaIncorporacion = $i['dtmFechaIncorporacion'];
                    $instructor->dtmFechaBaja = $i['dtmFechaBaja'];
                    $instructor->dtmFechaVigencia = $i['dtmFechaVigencia'];
                    $instructor->strGenero = $i['strGenero'];
                    $instructor->_intIdInst = $i['_intIdInst'];
                    $instructor->listTelefonoModel = null;
                    $instructor->listCorreoModel = null;
                    array_push($instructores, $instructor);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($instructores).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){

        }
        finally{

        }
    }
    public function actualizar(){

    }
    public function deshabilitar($id){
        try{
            $db = new mysql();
            $sql = "UPDATE usuarios SET intBanderaActivo = 0 WHERE intIdUsuario = '{$id}'";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Usuario deshabilitado"}';
            else
                return '{"status":"false", "msg":"No se pudo deshabilitar el usuario especificado"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"Ha ocurrido un error: '.$ex->getMessage().'"}';
        }
        finally{

        }
    }

    public function habilitar($id){
        try{
            $db = new mysql();
            $sql = "UPDATE usuarios SET intBanderaActivo = 1 WHERE intIdUsuario = '{$id}'";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Usuario habilitado"}';
            else
                return '{"status":"false", "msg":"No se pudo habilitar el usuario especificado"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"Ha ocurrido un error: '.$ex->getMessage().'"}';
        }
        finally{

        }
    }
}

//$instructor = new Instructor();
/*$instructor->strPass = "12345";
$instructor->strNombreUsuario = "saul";
$instructor->strNombre = "Saul  ";
$instructor->strApPaterno = "Arroyo";
$instructor->strApMaterno = "Castillo";
$instructor->dtmFechaNacimiento = "1997-10-14";
$instructor->dtmFechaIncorporacion = "2018-09-07";
$instructor->dtmFechaVigencia = "2018-09-30";
$instructor->strGenero = "HOMBRE";*/
//echo $instructor->registrarNuevo();
//echo $instructor->habilitar(5);
//echo $instructor->deshabilitar(5);
//echo $instructor->obtenerTodos();
//$instructor->login("admin@itsz.com", "1235");

?>
