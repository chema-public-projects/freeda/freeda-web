<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'/MySQL/MySQL.php';

class Lugar{
    //variables globales
    public $intIdLugar = 0;
    public $strNombreLugar = "";
    public $strDireccion = "";
    public $strUrlMapa = "";
    public $dcmLatitud = 0.0;
    public $dcmLongitud = 0.0;
    public $intZoom = 0;
    public $intBanderaActivo = 0;

    //Actividades con las que un lugar puede estar relacionado
    public $listActividadModel = [];

    //constructor
    public function __construct(){

    }

    public function agregarLugar(){
        $db = new mysql();
        try{            
            $sql = "INSERT INTO lugares (strNombreLugar,
                                        strDireccion,
                                        strUrlMapa,
                                        dcmLatitud,
                                        dcmLongitud,
                                        intZoom) 
                                VALUES ('{$this->strNombreLugar}',
                                        '{$this->strDireccion}',
                                        '{$this->strUrlMapa}',
                                        '{$this->dcmLatitud}',
                                        '{$this->dcmLongitud}',
                                        '{$this->intZoom}')";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Registro realizado correctamente"}';
            else
                return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function obtenerLugares(){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM lugares";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $lugares = [];//almacena los lugares obtenidos
                foreach($result as $r){
                    $lugar = new Lugar();
                    $lugar->intIdLugar = $r['intIdLugar'];
                    $lugar->strNombreLugar = $r['strNombreLugar'];
                    $lugar->strDireccion = $r['strNombreLugar'];
                    $lugar->strUrlMapa = $r['strUrlMapa'];
                    $lugar->dcmLatitud = $r['dcmLatitud'];
                    $lugar->dcmLongitud =$r['dcmLongitud'];
                    $lugar->intZoom = $r['intZoom'];
                    $lugar->intBanderaActivo = $r['intBanderaActivo'];
                    array_push($lugares, $lugar);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($lugares).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function obtenerLugaresActividad($intIdAct){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM lugares, actividades_lugares 
                            WHERE lugares.intIdLugar = actividades_lugares._intIdLugar && 
                            actividades_lugares._intIdAct = '{$intIdAct}'";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $lugares = [];//almacena los lugares obtenidos
                foreach($result as $r){
                    $lugar = new Lugar();
                    $lugar->intIdLugar = $r['intIdLugar'];
                    $lugar->strNombreLugar = $r['strNombreLugar'];
                    $lugar->strDireccion = $r['strNombreLugar'];
                    $lugar->strUrlMapa = $r['strUrlMapa'];
                    $lugar->dcmLatitud = $r['dcmLatitud'];
                    $lugar->dcmLongitud =$r['dcmLongitud'];
                    $lugar->intZoom = $r['intZoom'];
                    $lugar->intBanderaActivo = $r['intBanderaActivo'];
                    array_push($lugares, $lugar);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($lugares).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }
}

//$lugar = new Lugar();
/*$lugar->strNombreLugar = "Prueba";
$lugar->strDireccion = "Prueba";
$lugar->strUrlMapa = "https:www.google.com";
$lugar->dcmLatitud = 1.1;
$lugar->dcmLongitud = 0.1;
$lugar->intZoom = 5;
echo $lugar->agregarLugar();*/
//echo $lugar->obtenerLugares();
?>
