<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'MySQL/MySQL.php';
require_once $path_models.'Lugar.php';

class Actividad{
    //variables globales
    public $intIdActividad = 0;
    public $strNombreActividad = "";
    public $strDescripcion = "";
    public $intTotalHoras = 0;
    public $intCreditos = 0;
    public $intBanderaDisponibilidadInscripcion = 0;
    public $intBanderaActiva = 0;
    public $intCupo = 0;
    public $_intIdCamp = 0;
    public $_intIdTipo = 0;
    
    public $intIdTipoAct = 0;
    public $strNombreTipo = "";

    public $listLugares = [];
    public $intIdLugar = 0;

    //constructor
    public function __construct(){

    }

    public function obtenerTiposActividad(){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM tipos_actividad";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $tipos = [];//almacena los lugares obtenidos
                foreach($result as $r){
                    $tipo = new Actividad();
                    $tipo->intIdTipoAct = $r['intIdTipoAct'];
                    $tipo->strNombreTipo = $r['strNombreTipo'];
                    $tipo->_intIdTipo = $tipo->intIdTipoAct;
                    array_push($tipos, $tipo);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($tipos).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function agregarTipoActividad(){
        try{
            $db = new mysql();
            $sql = "INSERT INTO tipos_actividad(strNombreTipo) VALUES ('{$this->strNombreTipo}')";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Registro realizado correctamente"}';
            else
                return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function crearActividad(){
        $mysql = new mysql();
        try{
            $db = $mysql->getPDOConnection();
            $db->beginTransaction();
            $sql = "INSERT INTO actividades (strNombreActividad,
                                        strDescripcion,
                                        intTotalHoras,
                                        intCreditos,
                                        intBanderaDisponibilidadInscripcion,
                                        intBanderaActiva,
                                        intCupo,
                                        _intIdCamp,
                                        _intIdTipo) 
                                VALUES ('{$this->strNombreActividad}',
                                        '{$this->strDescripcion}',
                                        '{$this->intTotalHoras}',
                                        '{$this->intCreditos}',
                                        '{$this->intBanderaDisponibilidadInscripcion}',
                                        '{$this->intBanderaActiva}',
                                        '{$this->intCupo}',
                                        '{$this->_intIdCamp}',
                                        '{$this->_intIdTipo}')";
            $stmt = $db->prepare($sql);
            if($stmt->execute() > 0){
                if($stmt->rowCount() > 0){
                    $idActividad = $db->lastInsertId();
                    $sql_2 = "INSERT INTO actividades_lugares(_intIdAct, _intIdLugar) VALUES ('{$idActividad}', '{$this->intIdLugar}')";
                    $stmt_2 = $db->prepare($sql_2);
                    if($stmt_2->execute() > 0){
                        if($stmt->rowCount() > 0){
                            $db->commit();
                            return '{"status":"ok", "msg":"Registro realizado correctamente"}';                    
                        }
                        else{
                            $db->rollback();
                            return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
                        }
                    }
                    else{
                        $db->rollback();
                        return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
                    }
                }
                else{
                    $db->rollback();
                    return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
                }
            }
            else{
                $db->rollback();
                return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
            }
            
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' . $ex->getMessage() . '"}';
        }
        finally{
            $db = null;
        }
    }

    public function obtenerActividades(){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM actividades, tipos_actividad WHERE actividades._intIdTipo = tipos_actividad.intIdTipoAct";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $actividades = [];//almacena los lugares obtenidos
                foreach($result as $r){
                    $actividad = new Actividad();
                    $actividad->intIdAct = $r['intIdAct'];
                    $actividad->strNombreActividad = $r['strNombreActividad'];
                    $actividad->strDescripcion = $r['strDescripcion'];
                    $actividad->intTotalHoras = $r['intTotalHoras'];
                    $actividad->intCreditos = $r['intCreditos'];
                    $actividad->intBanderaDisponibilidadInscripcion = $r['intBanderaDisponibilidadInscripcion'];
                    $actividad->intBanderaActiva = $r['intBanderaActiva'];
                    $actividad->intCupo = $r['intCupo'];
                    $actividad->_intIdCamp = $r['_intIdCamp'];
                    $actividad->_intIdTipo = $r['_intIdTipo'];
                    $actividad->intIdTipoAct = $r['intIdTipoAct'];
                    $actividad->strNombreTipo = $r['strNombreTipo'];
                    $lugar = new Lugar();
                    $actividad->listLugares = json_decode($lugar->obtenerLugaresActividad($actividad->intIdAct));
                    array_push($actividades, $actividad);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($actividades).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }
}
//$actividad = new Actividad();

//$actividad->strNombreActividad = "Prueba";
//$actividad->strDescripcion = "Actividad de prueba";
//$actividad->intTotalHoras = "50";
//$actividad->intCreditos = "1";
//$actividad->intBanderaDisponibilidadInscripcion = "1";
//$actividad->intCupo = "25";
//$actividad->_intIdCamp = "1";
//$actividad->_intIdTipo = "1";
//$actividad->intIdLugar = 1;
                                        
//echo $actividad->agregarTipoActividad();
//echo $actividad->crearActividad();
//echo $actividad->obtenerActividades();
//echo  $actividad->obtenerTiposActividad();
/*
$status = new Status();
$status->strNombreStatus = "INACTIVA";
$status->strAbrevStatus = "I";
echo $status->crearStatus();
echo $status->obtenerStatus();*/
?>
