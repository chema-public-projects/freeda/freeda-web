<?php
  //$root = realpath($_SERVER["DOCUMENT_ROOT"]);
  //require_once "models/MySQL/MySQL.php";
  //require_once "models/General/cls_Mod_General.php";
  
  class Actividad extends cls_Mod_General{
    protected $TABLA_ACTIVIDADES;

    #CONSTRUCTOR
    public function __construct(){
      //define("$this->TABLA_ACTIVIDADES", "actividades");
      $this->TABLA_ACTIVIDADES = "actividades";
      $this->objMySQL = new MySQL();
      
    }
    
    #METODOS

    public function mtdAcreditarAlumnos($idActividad, $noControl, $blAcredita){
      $strSQL = "";
      if($blAcredita > 0){
        $strSQL = "UPDATE alumnos_actividades 
                    SET intBanderaInscrito = '0' 
                    WHERE alumnos_actividades._intIdAct = '{$idActividad}' 
                    AND alumnos_actividades._strNoControl = '{$noControl}'";
      }
      else{
        $strSQL = "UPDATE alumnos_actividades 
                    SET intBanderaInscrito = '1' 
                    WHERE alumnos_actividades._intIdAct = '{$idActividad}' 
                    AND alumnos_actividades._strNoControl = '{$noControl}'";
      }
        $this->objMySQL = new MySQL();
        
        //echo $strSQL;
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        $this->objMySQL = null;

          $strSQL = "UPDATE alumnos_actividades 
                    SET intBanderaAcredita = '{$blAcredita}' 
                    WHERE alumnos_actividades._intIdAct = '{$idActividad}' 
                    AND alumnos_actividades._strNoControl = '{$noControl}'";
        //echo $strSQL;
        $this->objMySQL = new MySQL();
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        $this->objMySQL = null;
        return $this->objResult;
        $this->objResult = null;
    }


    /*
     * Author: SAC
     * Date: 28/11/2016
     * Description: Método para insertar datos  de las actividades en la base de datos.Debido a que la relacion de las estidades Actividades y Lugares es de muchos a muchos, existe una tabla intermedia, por lo cual se debe insertar un registro en esta última.
     * Parameters: $strNombreActividad, $strCategoria, $strDescripcion, $intTotalHoras, $intCreditos, $intBanderaDisponibilidadInscripcion, $intBanderaActiva, $intCupo, $_intIdCampus, $_intIdLugar
     * Return: $objResult
     */
     public function mtdRegistrarActividad($strNombreActividad, $strCategoria, $intIdInstructor, $intIdLugar, $intIdCamp, $strDescripcion, $intTotalHoras, $intCreditos, $intCupo, $intDisponibilidad, $intEstadoActiva){

      $strSQL = "INSERT INTO actividades (strNombreActividad, 
                                          strCategoria, 
                                          strDescripcion, 
                                          intTotalHoras, 
                                          intCreditos, 
                                          intBanderaDisponibilidadInscripcion, 
                                          intBanderaActiva, 
                                          intCupo, 
                                          _intIdCamp) VALUES 
                                          ('{$strNombreActividad}', 
                                            '{$strCategoria}', 
                                            '{$strDescripcion}', 
                                            '{$intTotalHoras}', 
                                            '{$intCreditos}', 
                                            '{$intDisponibilidad}', 
                                            '{$intEstadoActiva}', 
                                            '{$intCupo}', 
                                            '{$intIdCamp}')";
      $actividadId = $this->objMySQL->mtdLastIdRegistro($strSQL);
      if($actividadId < 1)
        return false;
      else{
        $this->objMySQL = null;
        $this->objMySQL = new MySQL();
        $strSQL = "INSERT INTO actividades_lugares (_intIdAct, _intIdLug) VALUES ('{$actividadId}', '{$intIdLugar}');";
        $result = $this->objMySQL->mtdConsultaGenerica($strSQL);
        if($result === true){
          $this->objMySQL = null;
          $this->objMySQL = new MySQL();
          $strSQL = "INSERT INTO actividades_instructores (_intIdAct, _strIdInst) VALUES ('{$actividadId}', '{$intIdInstructor}');";
          $res = $this->objMySQL->mtdConsultaGenerica($strSQL);
          if($res === true){
            return true;
          }
          elseif ($res === false) {
            return false;
          }
          else
            return false;
        }
        elseif($result === false)
          return false;
        else
          return false;
      }
     }
     
     /*
      * Author: JMCL
      * Date: 28/11/2016
      * Description: Obtiene sólo las actividades que se encuentran activas
      * Parameters: $intId
      * Return: $objResult
      */
     public function mtdObtenerActividadesActivas(){
        $strSQL = "SELECT * FROM actividades, actividades_lugares, lugares, actividades_instructores, instructores, campus WHERE actividades.intBanderaActiva = 1 AND actividades_lugares._intIdAct = actividades.intIdAct AND lugares.intIdLug = actividades_lugares._intIdLug AND actividades_instructores._intIdAct = actividades.intIdAct AND instructores.strUsuarioInstr = actividades_instructores._strIdInst AND actividades._intIdCamp = campus.intIdCamp";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult;//hay datos
        elseif($this->objResult < 1)
          return 0;//no hay datos
        else
          return false;//error inesperado
        
        $this->objResult = null;
     }
     
     /*
       * Author: JMCL
       * Date: 30/01/2017
       * Description: Obtiene aquella actividades que están disponibles para incripción, además de los datos que relacionan al alumno con la actividad
       * Parameters: $strNoControl
       * Return $objResult
       */
      public function mtdObtenerActividadesDisponiblesInscripcion(){
        $strSQL = "SELECT actividades.intId, actividades.strNombreActividad, actividades.strDescripcion, lugares.strNombreLugar FROM actividades, actividades_lugares, lugares WHERE actividades.intBanderaDisponibilidadInscripcion = 1 AND actividades_lugares._intIdActividad = actividades.intId AND actividades_lugares._intIdLugar = lugares.intId;";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult;//hay datos
        elseif($this->objResult < 1)
          return 0;//no hay datos
        else
          return false;//error inesperado
        
        $this->objResult = null;
      }
      
     public function mtdObtenerDetallesActividad($intId){
      //$strSQL = "SELECT lugares.*, actividades.* FROM actividades, actividades_lugares, lugares WHERE actividades.intId = '{$intId}' AND actividades_lugares._intIdActividad = actividades.intId AND actividades_lugares._intIdLugar = lugares.intId GROUP BY lugares.intId";
      $strSQL = "SELECT lugares.*, 
                actividades_lugares.*,
                campus.strNombreCampus, 
                campus.intIdCamp, 
                actividades_instructores.*,
                CONCAT(instructores.strNombre, ' ', instructores.strApPaterno, ' ', instructores.strApMaterno) AS strNombreInstructor,
                actividades.* 
              FROM actividades, 
                  actividades_lugares, 
                  lugares, 
                  campus,
                  actividades_instructores,
                  instructores
              WHERE actividades.intIdAct = '{$intId}' 
                AND actividades_lugares._intIdAct = actividades.intIdAct 
                AND actividades_lugares._intIdLug = lugares.intIdLug 
                AND campus.intIdCamp = actividades._intIdCamp
                AND actividades_instructores._intIdAct = actividades.intIdAct
                AND actividades_instructores._strIdInst = instructores.strUsuarioInstr";
      $res = $this->objMySQL->mtdConsultaGenerica($strSQL);
        if($res > 0)
          return $res;//hay datos
        elseif($res < 1)
          return 0;//no hay datos
        else
          return false;//error inesperado
        
          $res = null;
     }
     
    /*
     * Author: AMJ
     * Date: 04/04/2017
     * Description: Modifica los datos de una actividad. Realiza cambios en la tabla actividades y lugares actividades
     * Parameters: $intId, $strNombreActividad, $strCategoria, $strDescripcion, $intTotalHoras, $intCreditos, $intBanderaDisponibilidadInscripcion, $intBanderaActiva, $intCupo, $_intIdCampus, $lugarActual, $nuevoLugar
     * Return:$objResult
     */
     public function mtdModificarActividad($objActividad,$oldCamp, $oldLug, $oldInst){
      //Modificar datos en la tabla actividades
      $strSQL = "UPDATE ".$this->TABLA_ACTIVIDADES." 
                SET strNombreActividad = '{$objActividad->strNombreActividad}', 
                    strCategoria = '{$objActividad->strCategoria}', 
                    strDescripcion = '{$objActividad->strDescripcion}', 
                    intTotalHoras = '{$objActividad->intTotalHoras}', 
                    intCreditos = '{$objActividad->intCreditos}', 
                    intBanderaDisponibilidadInscripcion = '{$objActividad->intBanderaDisponibilidadInscripcion}', 
                    intBanderaActiva = '{$objActividad->intBanderaActiva}', 
                    intCupo = '{$objActividad->intCupo}', 
                    _intIdCamp = '{$objActividad->_intIdCamp}' 
                WHERE intIdAct = '{$objActividad->intIdAct}';";
                
      $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
      if($this->objResult > 0 ){//Modificación exitosa
        //Modificar datos en la tabla actividades_lugares
        $strSQL = "UPDATE actividades_lugares 
                    SET _intIdLug = '{$objActividad->_intIdLug}' 
                    WHERE _intIdAct = '{$objActividad->intIdAct}' 
                    AND _intIdLug = '{$oldLug}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
      }
      if($this->objResult > 0 ){//Modificación exitosa
        //Modificar datos en la tabla actividades_lugares
        $strSQL = "UPDATE actividades_instructores
                    SET _strIdInst = '{$objActividad->_strIdInst}' 
                    WHERE _intIdAct = '{$objActividad->intIdAct}' 
                    AND _strIdInst = '{$oldInst}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);

        $this->objResult = true;
      }
      else//Modificación fallida
        $this->objResult = false;
        
      return $this->objResult;
      $this->objResult = null;
     }
     
      /*
     * Author: SAC
     * Date: 21/11/2016
     * Description: Método para obtener los alumnos inscritos en las actividades correspondientes.
     * Parameters: $intId, $strNombreActividad, $strCategoria, $strDescripcion, $intTotalHoras, $intCreditos, $intBanderaDisponibilidadInscripcion, $intBanderaActiva, $intCupo, $_intIdCampus
     * Return:$objResult
     */
      public function mtdObtenerAlumnosPorActividad($intId){
        $strSQL = "SELECT alumnos_actividades.intBanderaAcredita, alumnos_actividades._intIdAct, alumnos.strNombre, alumnos.strApPaterno, alumnos.strApMaterno, alumnos.strNoControl FROM alumnos_actividades INNER JOIN alumnos ON  alumnos.strNoControl = alumnos_actividades._strNoControl AND   alumnos_actividades._intIdAct = '{$intId}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
      }
      
      /*
       * Author: JMCL
       * Date: 27/11/2016
       * Description: Método para cambiar el estado de la actividad como inactiva.
       * Parameters: $intId
       * Return: $objResult
       */
      public function mtdDesactivarActividad($intId){
        $strSQL = "UPDATE actividades SET intBanderaActiva = 0, intBanderaDisponibilidadInscripcion = 0 WHERE intIdAct = '{$intId}'";
        //echo $strSQL;
        //echo $strSQL;
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
      }
      
      /*
      * Author: JMCL & SAC 
      * Date: 29/11/2016
      * Description: Obtiene sólo las actividades que se encuentran inactivas
      * Parameters: $intId
      * Return: $objResult
      */
     public function mtdObtenerActividadesInactivas(){
        //$strSQL = "SELECT * FROM ".$this->TABLA_ACTIVIDADES." WHERE intBanderaActiva = 0";
        $strSQL = "SELECT * FROM actividades, actividades_lugares, lugares, actividades_instructores, instructores, campus WHERE actividades.intBanderaActiva = 0 AND actividades_lugares._intIdAct = actividades.intIdAct AND lugares.intIdLug = actividades_lugares._intIdLug AND actividades_instructores._intIdAct = actividades.intIdAct AND instructores.strUsuarioInstr = actividades_instructores._strIdInst AND actividades._intIdCamp = campus.intIdCamp";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
     }
     
     /*
       * Author: JMCL
       * Date: 27/11/2016
       * Description: Método para cambiar el estado de la actividad como inactiva.
       * Parameters: $intId
       * Return: $objResult
       */
      public function mtdActivarActividad($intId){
        $strSQL = "UPDATE actividades SET intBanderaActiva = 1, intBanderaDisponibilidadInscripcion = 1 WHERE intIdAct = '{$intId}'";
        //echo $strSQL;
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
      }
      
      /*
       * Author: RCS
       * Date: 04/04/2017
       * Description: Obtiene las actividades (y otros datos generales) que se encuentran disponibles para inscripción
       * Parameters: none
       * Return: $objResult
       */
      public function mtdObtenerActividadesDisponibles($_intIdCampus){
        //$strSQL = "SELECT actividades.strNombreActividad FROM actividades, alumnos_actividades, alumnos WHERE alumnos_actividades._strIdAlumno = '{$strNoControl}' and alumnos_actividades.intBanderaInscrito = 0 and alumnos_actividades.intBanderaAcredita = 0 and alumnos_actividades._intIdActividad = actividades.intId GROUP BY actividades.intId";
        //$strSQL = "SELECT actividades.strNombreActividad, actividades.intId FROM actividades, alumnos_actividades WHERE actividades.intId != alumnos_actividades._intIdActividad and '146W0370' = alumnos_actividades._strIdAlumno GROUP BY actividades.intId;";
        /*$strSQL = "SELECT act.intId, act.strNombreActividad, alu.strNoControl
        from actividades as act inner JOIN alumnos_actividades as ala  
        on act.intId =  ala._intIdActividad INNER JOIN alumnos as alu 
        on ala._strIdAlumno = alu.strNoControl where act.intBanderaDisponibilidadInscripcion = 1
        AND alu.strNoControl = ala._strIdAlumno AND ala.intBanderaInscrito != 1 AND alu.strNoControl = '{$strNoControl}'";*/
        //$strSQL = "SELECT lugares.*, actividades.* FROM actividades, actividades_lugares, lugares WHERE actividades.intBanderaDisponibilidadInscripcion = 1 AND actividades.intBanderaActiva = 1 AND actividades_lugares._intIdActividad = actividades.intId AND actividades_lugares._intIdLugar = lugares.intId GROUP BY actividades.intId";
        //$strSQL = "SELECT lugares.*, actividades.* FROM actividades, actividades_lugares, lugares WHERE actividades.intBanderaDisponibilidadInscripcion = 1 AND actividades.intBanderaActiva = 1 AND actividades_lugares._intIdActividad = actividades.intId AND actividades_lugares._intIdLugar = lugares.intId";
        //$strSQL = "SELECT lugares.*, actividades.*, campus.* FROM actividades, actividades_lugares, lugares, campus WHERE actividades.intBanderaDisponibilidadInscripcion = 1 AND actividades.intBanderaActiva = 1 AND actividades_lugares._intIdAct = actividades.intIdAct AND actividades_lugares._intIdLug = lugares.intIdLug AND campus.intIdCamp = actividades._intIdCamp";
        $strSQL = "SELECT lugares.*, actividades.*, campus.*, instructores.*, actividades_instructores.*, lugares.strDireccion AS strDireccionLugar FROM actividades, actividades_lugares, lugares, campus, instructores, actividades_instructores WHERE actividades.intBanderaDisponibilidadInscripcion = 1 AND actividades.intBanderaActiva = 1 AND actividades_lugares._intIdAct = actividades.intIdAct AND actividades_lugares._intIdLug = lugares.intIdLug AND campus.intIdCamp = actividades._intIdCamp AND actividades.intIdAct = actividades_instructores._intIdAct AND instructores.strUsuarioInstr = actividades_instructores._strIdInst AND actividades._intIdCamp = '{$_intIdCampus}'";
        $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult; //hay datos
        elseif($this->objResult < 1)
          return 0; //no hay datos
        else
          return false;//error inesperado
     }
     
     /*
       * Author: RCS
       * Date: 04/04/2017
       * Description: Obtiene los ID de las actividades en las que se encuentra inscrito un alumno.
       * Parameters: $strNoControl
       * Return: $objResult
       */
     public function mtdObtenerActividadesAlumnoInscrito($strNoControl){
      //$strSQL = "SELECT _intIdActividad FROM alumnos_actividades WHERE _strIdAlumno = '{$strNoControl}' AND intBanderaInscrito = 1";
      //$strSQL = "SELECT * FROM actividades as ACT JOIN alumnos_actividades as ALACT WHERE ALACT._strNoControl = '{$strNoControl}' AND ALACT.intBanderaInscrito = 1 AND ACT.intIdAct = ALACT._intIdAct;";
      $strSQL = "SELECT * FROM actividades AS ACT, alumnos_actividades AS ALA, alumnos AS AL WHERE ALA._strNoControl = '{$strNoControl}' and ALA.intBanderaInscrito = 1 AND ALA.intBanderaAcredita = 0 and ALA._intIdAct = ACT.intIdAct GROUP BY ACT.intIdAct" ;
      //$strSQL = "SELECT alumnos_actividades._intIdActividad, actividades.strNombreActividad FROM alumnos_actividades, actividades WHERE alumnos_actividades._strIdAlumno = '{$strNoControl}' AND intBanderaInscrito = 1 AND actividades.intId = alumnos_actividades._intIdActividad";

      $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult; //hay datos
        elseif($this->objResult < 1)
          return 0; //no hay datos
        else
          return false;//error inesperado
     }

     /*
     *
     */
     public function mtdObtenerAcreditadas($strNoControl){
      $strSQL = "SELECT * FROM actividades AS ACT JOIN alumnos_actividades as ALACT WHERE ALACT._strNoControl = '{$strNoControl}' AND ALACT.intBanderaAcredita = 1 AND ACT.intIdAct = ALACT._intIdAct;";
      $objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
      return $objResult;
     }

}
?>