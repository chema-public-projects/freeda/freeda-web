<?php
	// $root = realpath($_SERVER["DOCUMENT_ROOT"]);
	// require_once "../../models/MySQL/MySQL.php";
	// require_once "../../models/General/cls_Mod_General.php";
	
	class Administrador extends cls_Mod_General{
		
		#CONSTRUCTOR
		public function __construct(){
			//define("TABLA_ADMINISTRADORES", "administradores");
			$this->objMySQL = new MySQL();
		}
		
		/*
		 * 
		 */
		public function mtdInicioSesion($strCURP, $intNIP){
			$strSQL = "SELECT * FROM administradores WHERE strCURP = '{$strCURP}' AND intNIP = '{$intNIP}' AND intBanderaSesionStatus = 0";
			$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL); 
			
			if($this->objResult > 0){
				$strSQL = "UPDATE administradores SET intBanderaSesionStatus = 1 WHERE strCURP = '{$strCURP}';";
				$this->objMySQL->mtdConsultaGenerica($strSQL);
			}
			
			return $this->objResult;
			$this->objResult = null;
		}
		
		/*
		 * Author: JMCL
		 * Date:
		 * Description:
		 * Parameters:
		 * Return:
		 */
		public function mtdValidarAdmin($strUsuario, $strPass){
			//$strSQl = "SELECT * FROM ".TABLA_ADMINISTRADORES." WHERE";
		$strSQL = "SELECT * FROM administradores WHERE strUsuario = '{$strUsuario}' AND strPass = '{$strPass}' ;";
        $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        echo $strSQL;
        return $this->objResult;
		}
		/*
		 * Author: JMCL
		 * Date: 13/03/2017
		 * Description; Método que inserta en la tabla administradores un valor 0 en el campo intBanderaSesionStatus para indicar que la sesión ha sido terminada.
		 * Parameters: none
		 * Return: none
		 */
		public function mtdCerrarSesion($strCURP){
			$strSQL = "UPDATE ".TABLA_ADMINISTRADORES." SET intBanderaSesionStatus = 0 WHERE strCURP = '{$strCURP}';";
			$this->objMySQL->mtdConsultaGenerica($strSQL);
		}

		public function mtdCambiarPass($nuevaPass, $strUsuario){
        $strSQL = "UPDATE administradores SET intBanderaPassCambiada = 1, strPass = '{$nuevaPass}' WHERE strUsuario = '{$strUsuario}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
      }

      public function mtdCrearAdministrador($strUsuario, $strPass, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNacimiento, $strNoTelefono, $strEmail){
      	$strSQL = "INSERT INTO administradores (strUsuario, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strTelefono, strPass, strEmail, intNivelAcceso) VALUES
      	 								('{$strUsuario}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNacimiento}', '{$strNoTelefono}', '{$strPass}', '{$strEmail}', 1)";
		$result = $this->objMySQL->mtdConsultaGenerica($strSQL);

		return $result;
      }


	}
?>