<?php
	require_once "../../models/MySQL/MySQL.php";
	require_once "../../models/General/cls_Mod_General.php";
	
	class cls_Mod_Reporte extends cls_Mod_General{
		#VARIABLES GLOBALES  
		
		#CONSTRUCTOR
		public function __construct(){
			define("TABLA_ALUMNOS", "alumnos");
			$this->objMySQL = new MySQL();
		}
		
		#METODOS
		/*
		* Author: JMCL
		* Date: 27/04/2017
		* Description: Método para obtener registros de alumnos para generar un reporte en excel
		* Parameters: $strCondiciones
		* Return: $objResult
		*/
		public function mtdObtenerDatosAlumnos($strCondiciones){
			$strSQL = "SELECT alumnos.strNoControl,
						CONCAT(alumnos.strNombre, ' ', alumnos.strApPaterno, ' ', strApMaterno) AS strNombre,
						alumnos._intIdCampus,
						alumnos._intIdCarrera,
						alumnos.intSemestre,
						alumnos.chrGenero,
						(SELECT COUNT(*)
							FROM alumnos_actividades
							WHERE alumnos_actividades._strIdAlumno = alumnos.strNoControl
							AND alumnos_actividades.intBanderaAcredita = 1)
							AS intNumActAcreditadas
						FROM alumnos";
			$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
			return $this->objResult;
		} 
		
		/*
		* Author: JMCL
		* Date: 30/04/2017
		* Description: Método para obtener registros de alumnos para generar un reporte en excel
		* Parameters: $strCondiciones
		* Return: $objResult
		*/
		public function mtdObtenerDatosInstructores($strCondiciones){
			$strSQL = " ";
			$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
			return $this->objResult;
		}
	}
?>