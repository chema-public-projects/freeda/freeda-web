<?php
    //$root = realpath($_SERVER["DOCUMENT_ROOT"]);  //root directorio de la raiz
    //require_once "../../models/MySQL/MySQL.php";
    //require_once "../../models/General/cls_Mod_General.php";
    
    class Instructor extends cls_Mod_General{
        
        #CONSTRUCTOR
        public function __construct (){
            define ("TABLA_INSTRUCTORES", "instructores");
            $this->objMySQL = new MySQL(); 
        }
        
        #METODOS
    
        /*
        * Autor: JMCL
        * Date: 17/08/2017
        */
        public function mtdObtenerInstructoresActivos(){
            $strSQL = "SELECT * FROM instructores WHERE intBanderaActivo = 1";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }

        /*
        * Autor: JMCL
        * Date: 18/08/2017
        */
        public function mtdObtenerInstructoresInactivos(){
            $strSQL = "SELECT * FROM instructores WHERE intBanderaActivo = 0";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }

        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para registrar un instructor. El return devolverá un valor booleano en función al resultado de la consulta.
         * Parameters: $strUsuarioInstr, $strPass, $strNombre, $strApPaterno, $strApMaterno, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail, $intBanderaActivo
         * Return: bool
         */
        public function mtdRegistrarInstructor($strUsuario, $strPass, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNac, $strTelefono, $strEmail){
            $strSQL = "";
            if(empty($strTelefono) && empty($strEmail))
                $strSQL = "INSERT INTO instructores (strUsuarioInstr, strPass, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strTelefono, strEmail) VALUES ('{$strUsuario}', '{$strPass}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNac}', 'NO HA SIDO ESPECIFICADO', 'NO HA SIDO ESPECIFICADO')";
            if(!(empty($strTelefono)) && (empty($strEmail)))
                $strSQL = "INSERT INTO instructores (strUsuarioInstr, strPass, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strTelefono, strEmail) VALUES ('{$strUsuario}', '{$strPass}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNac}', '{$strTelefono}', 'NO HA SIDO ESPECIFICADO')";
            if((empty($strTelefono)) && !(empty($strEmail)))
                $strSQL = "INSERT INTO instructores (strUsuarioInstr, strPass, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strTelefono, strEmail) VALUES ('{$strUsuario}', '{$strPass}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNac}', 'NO HA SIDO ESPECIFICADO', '{$strEmail}')";
            else
                $strSQL = "INSERT INTO instructores (strUsuarioInstr, strPass, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strTelefono, strEmail) VALUES ('{$strUsuario}', '{$strPass}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNac}', '{$strTelefono}', '{$strEmail}')";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }
        
        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para actualizar la información de un registro específico. El return devolverá un valor booleano en función al resultado de la consulta.
         * Parameters: $strUsuarioInstr, $strPass, $strNombre, $strApPaterno, $strApMaterno, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail, $intBanderaActivo
         * Return: $objResult
         */
        public function mtdModificarInstructor($strUsuarioInstr, $nombreCampo, $valorCampo){
            
            $strSQL = "UPDATE  ".TABLA_INSTRUCTORES." SET $nombreCampo = '{$valorCampo}' WHERE strUsuarioInstr = '{$strUsuarioInstr}'";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }
        
        public function  mtdValidarInstructor($strUsuarioInstr, $strPass){
            $strSQL = "SELECT * FROM ".TABLA_INSTRUCTORES." WHERE strUsuarioInstr = '{$strUsuarioInstr}' AND strPass = '{$strPass}'";
			$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
			return $this->objResult;
            $this->objResult = null;
        }
		
		public function mtdSolicitarAltaInstructor($strUsuarioInstr, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail, $strNombreActividad){
			$strSQL = "SELECT strUsuarioInstr from ".TABLA_INSTRUCTORES." WHERE strUsuarioInstr = '{$strUsuarioInstr}'";
			$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
			if($this->objResult > 0)
				return false;
			else{
				$this->objResult = null;
				$strSQL = "INSERT INTO instructores_aspirantes(strUsuarioInstr, strNombre, strApPaterno, strApMaterno, chrGenero, dtmFechaNacimiento, strDireccion, strTelefono, strEmail, strNombreActividad) values ('{$strUsuarioInstr}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$chrGenero}', '{$dtmFechaNacimiento}', '{$strDireccion}', '{$strTelefono}', '{$strEmail}', '{$strNombreActividad}');";
				$this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
				return $this->objResult;				
			}
			$this->objResult = null;
        }

        /*
        * Description:
        * Author:
        * Date: 23/12/2017
        * Parameters:
        * Returns:
        */
        public function mtdCambiarPass($nuevaPass, $strUsuario){
        $strSQL = "UPDATE instructores SET intBanderaPassCambiada = 1, strPass = '{$nuevaPass}' WHERE strUsuarioInstr = '{$strUsuario}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        }
    } 
    ?>