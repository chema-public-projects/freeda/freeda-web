<?php
    //$root = realpath($_SERVER["DOCUMENT_ROOT"]);
    //require_once "../../models/MySQL/MySQL.php";
    //require_once "../../models/General/cls_Mod_General.php";
    
    class Lugar extends cls_Mod_General{
        
        # CONSTRUCTOR
        public function __construct(){
            define("TABLA_LUGARES", "lugares");
            $this->objMySQL = new MySQL();
        }
        
        # METODOS
        
        /*
         * Author: JMAG
         * Date: 19/09/2016
         * Description: Método para hacer una inserción en la tabla Lugares.
         * Parameters: $strNombreLugar, $strDireccion, $strUrlMapa
         * Return: $objResult
         */
        public function mtdRegistrarLugar($strNombreLugar, $strDireccion, $strAltitud, $strLongitud, $intZoom){
            $strSQL = "INSERT INTO lugares  (strNombreLugar, strDireccion, decLatitud, decLongitud, intZoom) VALUES ('{$strNombreLugar}', '{$strDireccion}', '{$strAltitud}', '{$strLongitud}', '{$intZoom}')";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }

        /*
        * Author: JMAG
        * Date: 19/09/2016
        * Description: Método para modificar lugares.
        * Parameters: $intId, $strNombreLugar, $strDireccion, $strUrlMapa
        * Return: $objResult
        */
        public function mtdModificarLugar($intId, $strNombreLugar, $strDireccion, $strUrlMapa){
            $strSQL = "UPDATE ".TABLA_LUGARES." SET strNombreLugar = '{$strNombreLugar}' , strDireccion = '{$strDireccion}', strUrlMapa = '{$strUrlMapa}' WHERE intId = '{$intId}';";
            echo $strSQL;
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
            $this->objResult = null;
        }
    } 
?>