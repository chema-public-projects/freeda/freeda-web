<?php
  $root = realpath($_SERVER["DOCUMENT_ROOT"]);
  //require_once "models/Alumnos/ModAlumno.php";
  //require_once "models/MySQL/MySQL.php";
  //require_once "models/General/cls_Mod_General.php";
  
  class Alumno extends cls_Mod_General{
    
    #CONSTRUCTOR
    public function __construct(){
     define("TABLA_ALUMNOS","alumnos");
     define("TABLA_ALUMS_DATOS_COMPL", "alums_datos_compl");
     $this->objMySQL = new MySQL();
    }
    
    #METODOS
    /*
     * Author: AMJ
     * Date: 14/09/2016
     * Description: Método para insertar datos  de los alumnos en la base de datos.
     * Parameters: $strNoControl, $intNIP, $strNombre, $strApPaterno, $strApMaterno, $strDireccion, $intSemestre, $intFaltasAcumuladas, $strNumEmergencia, $strLimitaciones, $strNoTelefono, $intBanderaNIPCambiado, $intIdCampus, $intIdCarrera
     * Return: $objResult
     */
     public function mtdRegistrarAlumno($strNoControl, $intNIP, $strNombre, $strApPaterno, $strApMaterno, $strDireccion, $intSemestre, $intFaltasAcumuladas, $strNumEmergencia, $strLimitaciones, $strNoTelefono, $intBanderaNIPCambiado, $intIdCampus, $intIdCarrera){
      $strSQL = "INSERT INTO ".TABLA_ALUMNOS." (strNoControl, intNIP, strNombre, strApPaterno, strApMaterno, strDireccion, intSemestre, intFaltasAcumuladas, strNumEmergencia, strLimitaciones, strNoTelefono, intBanderaNIPCambiado, _intIdCampus, _intIdCarrera) VALUES ('{$strNoControl}', '{$intNIP}', '{$strNombre}', '{$strApPaterno}', '{$strApMaterno}', '{$strDireccion}', '{$intSemestre}', '{$intFaltasAcumuladas}', '{$strNumEmergencia}', '{$strLimitaciones}', '{$strNoTelefono}', '{$intBanderaNIPCambiado}', '{$intIdCampus}', '{$intIdCarrera}');";
      $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
      return $this->objResult;
      $this->objResult = null;
     }
     
    /*
     * Author: AMJ
     * Date: 14/09/2016
     * Description: Método para modificar los registros de los alumnos en la base de datos.
     * Parameters: $intId,$strNombre,$strApPaterno,$strApMaterno,$strNoControl,$strDireccion,$intIdCampus,$intIdCarrera
     * Return: $objResult
     */
     public function mtdModificarAlumno($strNoControl, $intNIP, $strNombre, $strApPaterno, $strApMaterno, $strDireccion, $intSemestre, $intFaltasAcumuladas, $strNumEmergencia, $strLimitaciones, $strNoTelefono, $intBanderaNIPCambiado, $intIdCampus, $intIdCarrera){
      $strSQL = "UPDATE ".TABLA_ALUMNOS." SET intNIP = '{$intNIP}', strNombre = '{$strNombre}', strApPaterno = '{$strApPaterno}', strApMaterno = '{$strApMaterno}', strDireccion ='{$strDireccion}', intSemestre = '{$intSemestre}', intFaltasAcumuladas = '{$intFaltasAcumuladas}', strNumEmergencia = '{$strNumEmergencia}', strLimitaciones = '{$strLimitaciones}', strNoTelefono = '{$strNoTelefono}', intBanderaNIPCambiado = '{$intBanderaNIPCambiado}', _intIdCampus = '{$intIdCampus}', _intIdCarrera = '{$intIdCarrera}' WHERE strNoControl = '{$strNoControl}';";
      $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
      return $this->objResult;
      $this->objResult = null;
     }
     
     /*
     * Author: JMCL
     * Date: 14/12/2016
     * Description: Obtiene los datos de un registro en donde coincidan el numero de control y la contraseña. 
     * Parameters: $strNoControl, $intNIP
     * Return: $objResult
     */
     public function mtdValidarAlumno($strNoControl, $strPass){
        //$strSQL = "SELECT alumnos.*, alums_datos_compl.* FROM ".TABLA_ALUMS_DATOS_COMPL." AS DATOS, ".TABLA_ALUMNOS." AS ALUM WHERE DATOS.strAlumNoControl = ALUM.strNoControl AND DATOS.strAlumNoControl = '{$strNoControl}' AND DATOS.strPass = '{$strPass}'";
      $strSQL = "SELECT ALUM.*, DATOS.* FROM alums_datos_compl AS DATOS, alumnos AS ALUM WHERE DATOS.strAlumNoControl = ALUM.strNoControl AND DATOS.strAlumNoControl = '{$strNoControl}' AND DATOS.strPass = '{$strPass}' AND ALUM.strNoControl = DATOS._strNoControlAlum";
        $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
     }
     
     /*
      * Author: JMCL
      * Date: 30/01/2017
      * Description: Selecciona las actividades en las que un alumno se encuentra inscrito pero no han sido liberadas
      * Parameters: $strNoControl
      * Return: $objResult
      */ 
     public function mtdObtenerActividadesCursando($strNoControl){
        //$strSQL = "SELECT actividades.intId, actividades.strNombreActividad FROM actividades, alumnos_actividades WHERE actividades.intId = alumnos_actividades._intIdActividad AND alumnos_actividades._strIdAlumno = '{$strNoControl}' AND intBanderaInscrito = 1 AND intBanderaAcredita = 0";
        $strSQL = "SELECT ACT.strNombreActividad FROM actividades AS ACT, alumnos_actividades AS ALA, alumnos AS AL WHERE ALA._strNoControl = '146W0371' and ALA.intBanderaInscrito = 1 AND ALA.intBanderaAcredita = 0 and ALA._intIdAct = ACT.intIdAct GROUP BY ACT.intIdAct" ;
        $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult;//hay datos
        elseif($this->objResult < 1)
          return 0;//no hay datos
        else
          return false;//error inesperado
        
        $this->objResult = null;
     }
     
     public function mtdObtenerActividadesLiberadas($strNoControl){
        $strSQL =  "SELECT actividades.strNombreActividad FROM actividades, alumnos_actividades, alumnos WHERE alumnos_actividades._strIdAlumno = '{$strNoControl}' and alumnos_actividades.intBanderaInscrito = 1 and alumnos_actividades.intBanderaAcredita = 1 and alumnos_actividades._intIdActividad = actividades.intId GROUP BY actividades.intId";
        $this->objResult =$this->objMySQL->mtdConsultaGenerica($strSQL);
        if($this->objResult > 0)
          return $this->objResult; //hay datos
        elseif($this->objResult < 1)
          return 0; //no hay datos
        else
          return false;//error inesperado
     }
     

     /*
     * Author: JMCL
     * Date: Modificación 10/08/2017
     * Description: 
     */
     public function mtdInscribirseActividad($_strNoControl, $_intIdActividad){
      /*$strSQLSP = "CALL do_inscription('{$_intIdActividad}' , '{$_strNoControl}',@b);";
      $this->objMySQL = null;
      $this->objMySQL = new MySQL();
      $this->objResult = $this->objMySQL->mtdEjecutarSP($strSQLSP);
      echo $this->objResult . " ";
      echo $this->objResult[0];
      $bandera =  $this->objMySQL->mtdEjecutarSP("SELECT @b as result");
      print_r($bandera);
      echo "El SP dice: ".$bandera['result'];
      return $bandera;
*/



        $strSQL_1 = "SELECT * FROM alumnos_actividades WHERE  _intIdAct = '{$_intIdActividad}' AND _strNoControl = '{$_strNoControl}' AND intBanderaInscrito = 1";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL_1);
        if($this->objResult < 1){///No está inscrito
          $strSQLCupo = "SELECT intCupo from actividades WHERE intIdAct = '{$_intIdActividad}'";
          $this->objMySQL = null;
          $this->objMySQL = new MySQL();
          $cupo = $this->objMySQL->mtdConsultaGenerica($strSQLCupo);
          
          if(intval($cupo[0][0]) > 0){//Hay cupo
            $this->objMySQL = null;
            $this->objMySQL = new MySQL();
            $strSQLBajarCupo = "UPDATE actividades SET intCupo = intCupo - 1 WHERE intIdAct = '{$_intIdActividad}'";
            $this->objMySQL->mtdConsultaGenerica($strSQLBajarCupo);

            $this->objMySQL = null;
            $this->objMySQL = new MySQL();
            $strSQL_2 = "INSERT INTO alumnos_actividades (intBanderaInscrito , _intIdAct , _strNoControl) VALUES ('1' , '{$_intIdActividad}' , '{$_strNoControl}')";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL_2);
            if($this->objResult > 0)
              return "true";
          }else{//No hay cupo
            return "nocupo";
          }
        }
        elseif($this->objResult > 0)//Ya está inscrito
          return "false"; 
        else
          return "error"; //error inesperado 
      }
      
      public function mtdDesinscribirseActividad($strNoControl, $intIdActividad){
        $strSQL = "DELETE FROM alumnos_actividades WHERE _strIdAlumno = '{$strNoControl}' AND _intIdActividad = '{$intIdActividad}' ";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
        $this->objResult = null;
      }

      public function mtdCambiarPass($nuevaPass, $strNoControl){
        $strSQL = "UPDATE alums_datos_compl SET intBanderaPassCambiada = 1, strPass = '{$nuevaPass}' WHERE strAlumNoControl = '{$strNoControl}'";
        $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
        return $this->objResult;
      }
     
  }
 ?>