<?php
    //$root = realpath($_SERVER['DOCUMENT_ROOT']);
    //require_once "../../models/MySQL/MySQL.php";
    //require_once "../../models/General/cls_Mod_General.php";
    
    class Aviso extends cls_Mod_General{
        # VARIABLES GLOBALES
        protected $TABLA_AVISOS;

        # CONSTRUCTOR
        public function __construct(){
            //define ("TABLA_AVISOS", "avisos");
            $this->TABLA_AVISOS = "avisos";
            $this->objMySQL = new MySQL();
        }
        
        # MÉTODOS
        
        /*
         * Author: JMCL
         * Date: 21/10/2016
         * Description: Método para crear un nuevo registro en la tabla Avisos.Adicionalmente inserta un registro en la tabla avisos_administradores o avisos_instructores según sea el caso.
         * Parameters: $strTipoUsuario, $strCURPUsuario, $strTitulo, $strDescripcion, $dtmFechaEmision, $bolBanderaGeneral, $intIdActividad
         * Return: objResult
         */
        public function mtdRegistrarAviso($strTipoUsuario, $strCURPUsuario, $strTitulo, $strDescripcion, $dtmFechaEmision, $bolBanderaGeneral, $intIdActividad){
            $strSQL = "INSERT INTO ".$this->TABLA_AVISOS." (strTitulo, strDescripcion, dtmFechaEmision, intBanderaGeneral, _intIdActividad) VALUES ('{$strTitulo}', '{$strDescripcion}', '{$dtmFechaEmision}', '{$bolBanderaGeneral}', '{$intIdActividad}');";
            $avisoId = $this->objMySQL->mtdLastIdRegistro($strSQL);
            $strSQL = "";
            if($avisoId < 1)
                $this->objResult = false;
            else{
                switch($strTipoUsuario){
                    case "ADMINISTRADOR":
                        $strSQL = "INSERT INTO avisos_administradores (_strCURPAdministrador, _intIdAvisos) VALUES ('{$strCURPUsuario}', '{$avisoId}')";
                        break;
                    case "INSTRUCTOR":
                        $strSQL = "INSERT INTO avisos_instructores (_strCURPInstructor, _intIdAvisos) VALUES ('{$strCURPUsuario}', '{$avisoId}')";
                        break; 
                }
                //$strSQL = "INSERT INTO ";
                $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
                if($this->objResult === true)
                    $this->objResult = true;
                else
                    $this->objResult = false;
            }
            return $this->objResult;
            $this->objResult = null;
        }
        public function mtdRegistrarAvisoAdmin($strTitulo, $strDescripcion, $dtmFechaEmision, $dtmFechaFin, $intBanderaGeneral, $_strIdUsuario, $_intIdAct){
            
            $strSQL = "INSERT INTO avisos_admins (strTitulo, strDescripcion, dtmFechaEmision, dtmFechaFin, intBanderaGeneral, _strIdAdmin, _intIdAct) ";
            $strSQL = $strSQL." VALUES ('{$strTitulo}', '{$strDescripcion}', '{$dtmFechaEmision}', '{$dtmFechaFin}', '{$intBanderaGeneral}', '{$_strIdUsuario}', '{$_intIdAct}')";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $strSQL;
        }
        public function mtdRegistrarAvisoInstructor($strTitulo, $strDescripcion, $dtmFechaEmision, $dtmFechaFin, $intBanderaGeneral, $_strIdUsuario, $_intIdAct){
            
            $strSQL = "INSERT INTO avisos_instructores (strTitulo, strDescripcion, dtmFechaEmision, dtmFechaFin, intBanderaGeneral, _strIdAdmin, _instIdAct) ";
            $strSQL = $strSQL." VALUES ('{$strTitulo}', '{$strDescripcion}', '{$dtmFechaEmision}', '{$dtmFechaFin}', '{$intBanderaGeneral}', '{$_strIdUsuario}', '{_intIdAct}')";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
        }
        
        /*
         * Author: JMCL
         * Date: 21/10/2016
         * Description: Método para modificar los datos de un registro de la tabla Avisos por medio de su ID.
         * Parameters: $intId, $strTitulo, $strDescripcion, $dtmFechaEmision, $bolBanderaGeneral, $intIdActividad
         * Return: $objResult
         */
        public function mtdModificarAviso($intId, $strTitulo, $strDescripcion, $dtmFechaEmision, $bolBanderaGeneral, $intIdActividad){
            $strSQL = "UPDATE ".$this->TABLA_AVISOS." SET strTitulo = '{$strTitulo}', strDescripcion = '{$strDescripcion}', dtmFechaEmision = '{$dtmFechaEmision}', bolBanderaGeneral = '{$bolBanderaGeneral}', _intIdActividad = '{$intIdActividad}' WHERE intId = '{$intId}';";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
            $this->objResult = null;
        }
        
        public function mtdObtenerAvisosAdministradores(){
            $strSQL = "SELECT avisos_admins.*, administradores.*
                       FROM avisos_admins, administradores 
                       WHERE avisos_admins._strIdAdmin = administradores.strUsuario GROUP BY avisos_admins.intIdAv";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
            $this->objResult = null;
        }
        
        public function mtdObtenerAvisosInstructores(){
            $strSQL = "SELECT avisos.*, instructores.strNombre, instructores.strApPaterno, instructores.strApMaterno 
                       FROM avisos, avisos_instructores, instructores 
                       WHERE avisos_instructores._strCURPInstructor = instructores.strCURP GROUP BY avisos.intId";
            $this->objResult = $this->objMySQL->mtdConsultaGenerica($strSQL);
            return $this->objResult;
            $this->objResult = null;
        }

        public function mtdContarAvisos(){
            $strSQL = "SELECT intIdAv from avisos_admins, avisos_intr";
        }
    }
?>