<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'MySQL/MySQL.php';

class Campus{
    //variables globales
    public $intIdCamp = 0;
    public $strNombreCampus = "";
    public $strDescripcion = "";
    public $dcmLatitud = 0.0;
    public $dcmLongitud = 0.0;
    public $intZoom = 0;
    public $strUrlMapa = "";
    public $intBanderaActivo = 0;

    //constructor
    public function __construct(){

    }

    public function agregarCampus(){
        $db = new mysql();
        try{            
            $sql = "INSERT INTO campus (strNombreCampus,
                                        strDescripcion,
                                        dcmLatitud,
                                        dcmLongitud,
                                        intZoom,
                                        strUrlMapa) 
                                VALUES ('{$this->strNombreCampus}',
                                        '{$this->strDescripcion}',
                                        '{$this->dcmLatitud}',
                                        '{$this->dcmLongitud}',
                                        '{$this->intZoom}',
                                        '{$this->strUrlMapa}')";
            $result = $db->mtdConsultaGenerica($sql);
            if($result)
                return '{"status":"ok", "msg":"Registro realizado correctamente"}';
            else
                return '{"status":"false", "msg":"El registro no se pudo llevar a cabo"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function obtenerCampus(){
        $db = new mysql();
        try{        
            $sql = "SELECT * FROM campus";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){//si el resultado no es falso o vacío
                $campus = [];//almacena los lugares obtenidos
                foreach($result as $r){ 
                    $c = new Campus();
                    $c->intIdCamp = $r['intIdCamp'];
                    $c->strNombreCampus = $r['strNombreCampus'];
                    $c->strDescripcion = $r['strDescripcion'];
                    $c->dcmLatitud = $r['dcmLatitud'];
                    $c->dcmLongitud = $r['dcmLongitud'];
                    $c->intZoom = $r['intZoom'];
                    $c->strUrlMapa = $r['strUrlMapa'];
                    $c->intBanderaActivo = $r['intBanderaActivo'];
                    array_push($campus, $c);
                }
                return '{"status":"ok", "msg":"Los datos fueron obtenidos correctamente", "data":'.json_encode($campus).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }
}

//$campus = new Campus();
//$campus->strNombreCampus = "Nogales";
//$campus->strDescripcion = "Campus en Nogales";
//$campus->dcmLatitud = "1.1";
//$campus->dcmLongitud = "1.2";
//$campus->intZoom = "6";
//$campus->strUrlMapa = "https://google.com";

//echo $campus->agregarCampus();
//echo $campus->obtenerCampus();

?>
