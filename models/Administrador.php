<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'/MySQL/MySQL.php';
require_once $path_models.'Usuario.php';
require_once $path_models.'Rol.php';

class Administrador extends Usuario{
    public $_intIdAdmin = 0;
    public $rolModel;


    public function login($user, $pass){

        try{
            $db = new mysql();
            $sql = "SELECT * FROM
                        usuarios,
                        administradores,
                        roles,
                        admins_roles
                    WHERE usuarios.strNombreUsuario = '{$user}'
                    AND usuarios.strPass = '{$pass}'
                    AND administradores._intIdAdmin = usuarios.intIdUsuario
                    AND admins_roles._intIdAdmin = usuarios.intIdUsuario
                    AND admins_roles._intIdRol = roles.intIdRol";
            $result = $db->mtdConsultaGenerica($sql);
            if($result){
                $administrador = new Administrador();
                $administrador->intIdUsuario = $result[0]['intIdUsuario'];
                $administrador->strPass = $result[0]['strPass'];
                $administrador->strNombreUsuario = $result[0]['strNombreUsuario'];
                $administrador->strNombre = $result[0]['strNombre'];
                $administrador->strApPaterno = $result[0]['strApPaterno'];
                $administrador->strApMaterno = $result[0]['strApMaterno'];
                $administrador->dtmFechaNacimiento = $result[0]['dtmFechaNacimiento'];
                $administrador->intBanderaActivo = $result[0]['intBanderaActivo'];
                $administrador->intBanderaPassCambiada = $result[0]['intBanderaPassCambiada'];
                $administrador->intBanderaSesionStatus = $result[0]['intBanderaSesionStatus'];
                $administrador->intIntentosLogin = $result[0]['intIntentosLogin'];
                $administrador->dtmUltimaSesion = $result[0]['dtmUltimaSesion'];
                $administrador->dtmFechaIncorporacion = $result[0]['dtmFechaIncorporacion'];
                $administrador->dtmFechaBaja = $result[0]['dtmFechaBaja'];
                $administrador->dtmFechaVigencia = $result[0]['dtmFechaVigencia'];
                $administrador->strGenero = $result[0]['strGenero'];
                $administrador->_intIdAdmin = $administrador->intIdUsuario;
                $administrador->rolModel = new Rol();
                $administrador->rolModel->intIdRol = $result[0]['intIdRol'];
                $administrador->rolModel->strNombreRol = $result[0]['strNombreRol'];
                return '{"status":"ok", "msg":"Los datos del usuario fueron obtenidos correctamente", "data":'.json_encode($administrador).'}';
            }
            else
                return '{"status":"false", "msg":"Usuario o contraseña incorrectos"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' + $ex->getMessage() + '"}';
        }
        finally{
            $db = null;
        }
    }

    public function registrarNuevo(){

    }
    public function obtenerPorId($id){

    }
    public function obtenerTodos(){

    }
    public function actualizar(){

    }
    public function deshabilitar($id){

    }
    public function habilitar($id){

    }
}

/*$administrador = new Administrador();
$administrador->login("admin@itsz.com", "1235");*/

?>
