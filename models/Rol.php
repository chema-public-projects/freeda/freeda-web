<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'/MySQL/MySQL.php';

class Rol{
    //variables globales
    public $intIdRol = 0;
    public $strNombreRol = "";

    public function agregarRol(){
        try{
            $mysql = new mysql();
            $db = $mysql->getPDOConnection();
            $sql = "INSERT INTO roles (strNombreRol) VALUES ('{$this->strNombreRol}')";
            $stmt = $db->prepare($sql);
            if($stmt->execute() > 0){
                if($stmt->rowCount() > 0)
                    return '{"status":"ok", "msg":"Registro realizado correctamente"}';
                return '{"status":"false", "msg":"No se pudo realizar el registro"}';
            }
                return '{"status":"false", "msg":"No se pudo realizar el registro"}';
        }
        catch(PDOException $ex){
            return '{"status":"error", "msg":"' . $ex->getMessage() . '"}';
        }
    }

    public function obtenerRoles(){
        try{
            $db = new mysql(); 
            $sql = "SELECT * FROM roles";
            $result = $db->mtdConsultaGenerica($sql);
            if($result > 0){
                $roles = [];
                foreach($result as $r){
                    $rol = new Rol();
                    $rol->intIdRol = $r['intIdRol'];
                    $rol->strNombreRol = $r['strNombreRol'];
                    array_push($roles, $rol);
                }
                    return '{"status":"ok", "msg":"Los datos se obtuvieron correctamente", "data":'.json_encode($roles).'}';
            }
            else
                return '{"status":"false", "msg":"Sin resultados"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"' . $ex->getMessage() . '"}';
        }
    }
}
 ?>
