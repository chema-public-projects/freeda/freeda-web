<?php
error_reporting(E_ERROR);
  session_start();
    if(!$_SESSION['TIPO_USUARIO']){
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Autentificación</title>
  <link rel="icon" type="image/png" href="v1/img/icon.png">
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
  <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Montserrat:400,700'>
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'-->
  <link rel="stylesheet" href="v1/css/loginInstr.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
  
  
<div class="container">
  <div class="info">
    <h1>Identifícate</h1><span>Portal de acceso al Panel de Instructores de Actividades Complementarias ITSZ</a></span>
    <h3 id="mensaje" style="color: #f00;"></h3>
  </div>
</div>
<div class="form" style="border-radius: 0px;">
  <div class="thumbnail"><img src="v1/img/icons/login_instr.svg"/></div>
  <form class="login-form" id="login-form" method="post">
    <input type="text" placeholder="USUARIO" style="text-align: center;" id="usuario"/>
    <input type="password" placeholder="CONTRASEÑA" style="text-align: center;" id="clave"/>
    <button id="btnLogin">Ingresar</button>
  </form>
</div>
  <script type="text/javascript" src='v1/js/jquery/jquery.js'></script>
  <script type="text/javascript" src="v1/js/main.js"></script>
  <script type="text/javascript" src="v1/js/loginInstr.js"></script>

</body>
</html>
<?php
  }
    else{
        switch($_SESSION['TIPO_USUARIO']){
            case "ALUMNO":
                echo '<script>window.location="InicioAlumnos"</script>';
            break;//END CASE
            case "ADMINISTRADOR":
                echo '<script>window.location="v1/PanelAdministrativo/Inicio"</script>';
            break;//END CASE
            case "INSTRUCTOR":
                echo '<script>window.location="instructores"</script>';
            break;//END CASE
            default:
              echo "Error";
              break;
        }//END SWITCH
    }
?>