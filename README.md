# SGAC - Sistema de Gestión de Actividades Complementarias
##### ITSZ Instituto Tecnológico Superior de Zongolica


SGAC ofrece una plataforma web que permite administrar la información de las actividades complementarias (AC) que se imparten en el ITSZ y permite a los alumnos el acceso a la información.

SGAC permite que administradores, instructores y alumnos interactúen en los procesos de gestión que implican las AC.

## [Manual de usuario] ('https://gitlab.com/denken/sistemas-y-plataformas/sgac/wikis/home')
## [Manual de desarrollador] ('https://gitlab.com/denken/sistemas-y-plataformas/sgac/wikis/home')

---
#### Administradores

A través de una interfaz de usuario clara y fácil de usar, los administradores pueden realizar las tareas de gestión necesarias tales como:

* Altas, bajas y edición de ACs
* Altas, bajas y edición de datos de Alumnos
* Altas, bajas y edicion de datos de Instructores
* Altas, bajas y edicion de datos de Administradores
* Publicación de información y comunicados dentro de la plataforma
---
#### Alumnos
Los alumnos por su parte tienen acceso a un panel en el cual pueden ver un resumen comprensible de la información relevante para ellos como:

* El progreso que llevan
* La información de las AC a las que han estado inscritos, incluyendo datos del instructor y la locación donde se llevan a cabo

---
#### Instructores
También los instructores tienen acceso a un panel desde el cual pueden: 

* Visualizar las AC que tienen a su cargo
* Visualizar a los alumnos inscritos en sus AC
* Evaluar a los alumnos para dictaminar los resultados finales
---
#### Funcionalidades extra

* La plataforma permite establecer permisos específicos para los usuarios del tipo administrador únicamente, de manera que un Súper Administrador es capaz de crear a otros Administradores con los mismos privilegios o con menos.
* Los alumnos tienen a su disposición la descarga de boletas en formato digital desde su panel así como la constancia final.
* Los paneles cuentan con un módulo básico de mensajería para establecer un canal de comunicación entre usuarios enlazados.
* Los administradores pueden realizar publicaciones de documento.
* Los administradores e instructores pueden emitir notificaciones.