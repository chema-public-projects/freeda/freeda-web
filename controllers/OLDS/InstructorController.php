<?php
    //$root = realpath($_SERVER["DOCUMENT_ROOT"]);  
    //require_once "../../models/Instructores/cls_Mod_Instructor.php";
    
    class cls_Ctrl_Instructor{
        
        # VARIABLES GLOBALES
        private $strCURP;
        private $strNombre;
        private $strApPaterno;
        private $strApMaterno;
        private $chrGenero;
        private $dtmFechaNacimiento;
        private $strDireccion;
        private $strTelefono;
        private $intNIP;
        private $strEmail;
        private $intBanderaNIPCambiado; //Puede ser que no se deba usar esto aqui, sino en la bd con un trigger
        private $strNombreActividad;
        private $objModel;
        private $objResult;
        
        # CONSTRUCTOR
        /*
         * Objeto del tipo Instructor.
         */
        public function __construct (){
            $this->objModel = new Instructor(); //objeto clsmondins cualquier metodo se tiene q obtener de la clase cldmodins... 
        }
        
        # METODOS
        
        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para obtener todos los registros desde la base de datos. El return devuelve un array bidimensional si la operación fue exitosa, de lo contrario viene vacío.
         * Parameters: none
         * Return $objResult
         */
        public function mtdObtenerTodosInstructores(){
            $this->objResult = $this->objModel->mtdObtenerTodosRegistros(TABLA_INSTRUCTORES);
            return $this->objResult;
            $this->objResult = null;  
        }

        /*
         *Author: JMCL
         * Date: 17/08/2017
         * Description: 
         * Parameters:
         * Return:
         */
        public function mtdObtenerInstructoresActivos(){
            $this->objResult = $this->objModel->mtdObtenerInstructoresActivos();
            return $this->objResult;
        }
        
        /*
        * Author: JMCL
        * Date: 12/08/2017
        * Description: 
        * Parameters:
        * Return:
        */
        public function mtdObtenerInstructoresInactivos(){
            $this->objResult = $this->objModel->mtdObtenerInstructoresInactivos();
            return $this->objResult;
        }

        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para obtener los datos de un instructor específico por medio de su id.
         * Parameters: $intId
         * Return: $objResult
         */
        public function mtdObtenerInstructorPorId($strUsuario){
            $this->objResult = $this->objModel->mtdObtenerRegistroPorId(TABLA_INSTRUCTORES, "strUsuarioInstr", $strUsuario);
            return $this->objResult;
        }
        
        /*
         * Author: RCS & JMCL
         * Date: 04/10/2016
         * Re: 14/08/2017
         * Description: Método para registrar un instructor. No recibe parámetros pero toma los valores mandados del método POST desde las vistas.
         * Parameters: none
         * Return: $objModel
         */
        public function mtdRegistrarInstructor($strUsuario, $strPass, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNac, $strTelefono, $strEmail){

            $this->objResult = $this->objModel->mtdRegistrarInstructor($strUsuario, $strPass, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNac, $strTelefono, $strEmail);
            return $this->objResult;
        }
        
        /*
         * Author: RCS
         * Date: 01/10/2016
         * Description: Método para actualizar los datos de un instructor. No recibe parámetros pero toma los valores mandados del método POST desde las vistas.
         * Parameters: none
         * Return: $objModel
         */
        public function mtdModificarInstructorPorId($strUsuarioInstr, $strPass, $strEmail, $strTelefono, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNac){
            
            if(strlen($strUsuarioInstr) > 0 && strlen($strPass) > 7 && strlen($strPass) < 16){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strPass', $strPass);
            }
            
            if(strlen($strUsuarioInstr) > 0 && strlen($strEmail) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strEmail', $strEmail);
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($strTelefono) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strTelefono', $strTelefono);
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($strNombre) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strNombre', $strNombre); 
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($strApPaterno) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strApPaterno', $strApPaterno); 
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($strApMaterno) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'strApMaterno', $strApMaterno); 
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($chrGenero) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'chrGenero', $chrGenero);
                echo $chrGenero;
            }
            if(strlen($strUsuarioInstr) > 0 && strlen($dtmFechaNac) > 0 ){
                $this->objModel = null;
                $this->objModel = new Instructor();
                $this->objResult = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'dtmFechaNacimiento', $dtmFechaNac);
            }
            
            return $this->objResult;
        }

        /*
        jmcl
        */
        public function mtdBajaInstructor($strUsuarioInstr){
            $result = $this->objModel->mtdModificarInstructor($strUsuarioInstr, 'intBanderaActivo', 0);
            return $result;
        }
        
        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para eliminar todos los datos de los instructores.
         * Parameters: none
         * Return: $objResult
         */
        public function mtdEliminarTodosInstructores(){
            $this->objResult = $this->objModel->mtdEliminarTodosRegistros(TABLA_INSTRUCTORES);
            return $this->objResult;
            $this->objResult = null;
        }
        
        /*
         * Author: RCS
         * Date: 04/10/2016
         * Description: Método para eliminar los datos de un instructor específico por medio de su id.
         * Parameters: $intId
         * Return: $objResult
         */
        public function mtdEliminarInstructorPorId($intId){
            $this->setIntId($intId);
            $this->objResult = $this->objModel->mtdEliminarInstructorPorId(TABLA_INSTRUCTORES, "strCURP", $this->getStrCURP());
            return $this->objResult;
            $this->objResult = null;
        }
        
        public function  mtdValidarInstructor($strUsuarioInstr, $strPass){
            $this->objResult = $this->objModel->mtdValidarInstructor($strUsuarioInstr, $strPass);
            return $this->objResult;
            $this->objResult = null;
        }
        
        /*
         * Description: Método que registra los datos de un aspitante a instructor.
         * Los parámetros deben asignarse a través de los métodos set. Son requeridos:
         * $strCURP, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail y $strNombreActividad
         * Author: JMCL
         * Date: 13/02/2017
         */
        public function mtdSolicitarAltaInstructor($strCURP, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail, $strNombreActividad){
            $this->objResult = $this->objModel->mtdSolicitarAltaInstructor($strCURP, $strNombre, $strApPaterno, $strApMaterno, $chrGenero, $dtmFechaNacimiento, $strDireccion, $strTelefono, $strEmail, $strNombreActividad);
            return $this->objResult;
            $this->objResult = null;
        }
        
        /*
        * Description: 
        * Parameters:
        * Author:
        * Date: 23/12/2017
        */
        public function mtdCambiarPass($nuevaPass, $strUsuario){
            $this->objResult = $this->objModel->mtdCambiarPass($nuevaPass,$strUsuario);
            return $this->objResult;
        }
       # GETTERS&SETTER
       
        function getStrCURP(){ return $this->strCURP; }
        function setStrCURP($strCURP){ $this->strCURP=$strCURP; }
        function getStrNombre(){ return $this->strNombre; }
        function setStrNombre($strNombre){ $this->strNombre=$strNombre; }
        function getStrApPaterno(){ return $this->strApPaterno; }
        function setStrApPaterno($strApPaterno){ $this->strApPaterno=$strApPaterno; }
        function getStrApMaterno(){ return $this->strApMaterno; }
        function setStrApMaterno($strApMaterno){ $this->strApMaterno=$strApMaterno; }
        function getChrGenero(){ return $this->chrGenero; }
        function setChrGenero($chrGenero){ $this->chrGenero = $chrGenero; }
        function getDtmFechaNacimiento(){ return $this->dtmFechaNacimiento; }
        function setDtmFechaNacimiento($dtmFechaNacimiento){ $this->dtmFechaNacimiento=$dtmFechaNacimiento; }
        function getStrDireccion(){ return $this->strDireccion; }
        function setStrDireccion($strDireccion){ $this->strDireccion=$strDireccion; }
        function getStrTelefono(){ return $this->strTelefono; }
        function setStrTelefono($strTelefono){ $this->strTelefono=$strTelefono; }
        function getStrEmail(){ return $this->strEmail; }
        function setStrEmail($strEmail){ $this->strEmail = $strEmail; }
        function getIntNIP(){ return $this->intNIP; }
        function setIntNIP($intNIP){ $this->intNIP=$intNIP; }
        function getStrNombreActividad(){ return $this->strNombreActividad; }
        function setStrNombreActividad($strNombreActividad){ $this->strNombreActividad = $strNombreActividad; }
        function getIntBanderaNIPCambiado(){ return $this->intBanderaNIPCambiado; }
        function setIntBanderaNIPCambiado($intBanderaNIPCambiado){ $this->intBanderaNIPCambiado=$intBanderaNIPCambiado; }
    }
?>