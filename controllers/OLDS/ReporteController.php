<?php
	require_once "../../models/Reportes/cls_Mod_Reporte.php";
	
	 class cls_Ctrl_Reporte{
		
		private $objModel;
		private $objResult;
		
		public function __construct(){
			$this->objModel = new cls_Mod_Reporte();	
		}
		
		/*
		 * Author: JMCL
		 * Date: 30/04/2017
		 * Description: Obtiene los registros de los alumnos para llenar el reporte de excel según las condiciones que se especifiquen.
		 * Parameters: $strCondiciones
		 * Return: $objResult
		 */
		public function mtdObtenerDatosAlumnos($strCondiciones){
			$this->objResult = $this->objModel->mtdObtenerDatosAlumnos($strCondiciones);
			return $this->objResult;
			$this->objResult = null;
		}
		
		/*
		 * Author: JMCL
		 * Date: 30/04/2017
		 * Description: Obtiene los registros de los instructores para llenar el reporte de excel según las condiciones que se especifiquen.
		 * Parameters: $strCondiciones
		 * Return: $objResult
		 */
		public function mtdObtenerDatosInstructores($strCondiciones){
			$this->objResult = $this->objModel->mtdObtenerDatosInstructores($strCondiciones);
			return $this->objResult;
			$this->objResult = null;
		}
	}
?>