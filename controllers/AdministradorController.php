<?php
$path_models = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."models".DIRECTORY_SEPARATOR;
$path_controllers = realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR;
require_once $path_models.'Administrador.php';
require_once $path_models.'Actividad.php';

$query = "";
if(isset($_POST['query']))
  $query = $_POST['query'];
if(isset($_GET['query']))
  $query = $_GET['query'];

switch ($query) {
    case 'login':
        $administradorCtrl = new AdministradorController();
        $result = $administradorCtrl->login($_GET['usuario'], $_GET['clave']);
        echo $result;
    break;
    case 'logout':
        session_start();
        session_destroy();
            echo '{"status":"ok", "msg":"Sesión finalizada"}';
    break;
  default:
    echo '{"status":"error", "msg":"La consulta no puede ser interpretada"}';
    break;
}

class AdministradorController{

    public function __construct(){

    }

    public function login($usuario, $clave){
        try{
            $administrador = new Administrador();
            $result = $administrador->login($usuario, $clave);
            $jsonResult = json_decode($result);
            if($jsonResult){
                if($jsonResult->status == "ok"){
                    session_start();
                    $_SESSION['ID_USUARIO'] = $jsonResult->data->intIdUsuario;
                    $_SESSION['TIPO_USUARIO'] = 'ADMINISTRADOR';
                    $_SESSION['USUARIO'] = $jsonResult->data->strNombreUsuario;
                    $_SESSION['NOMBRE'] = $jsonResult->data->strNombre;
                    $_SESSION['NOMBRE_COMPLETO'] = $jsonResult->data->strNombre." ".$jsonResult->data->strApPaterno." ".$jsonResult->data->strApMaterno;
                    $_SESSION['FLAG_PASS_CAMBIADA'] = intval($jsonResult->data->intBanderaPassCambiada);
                    $_SESSION['GENERO'] = $jsonResult->data->strGenero;
                    return $result;//retorna el string json completo
                }
                else//no se inicia sesion
                    return $result;//retorna el string json completo
            }
            else
                return '{"status":"error", "msg":"Error desconocido"}';
        }
        catch(Exception $ex){
            return '{"status":"error", "msg":"Ocurrió una excepción '.$ex->getMessage().'"}';
        }
    }
}

?>
